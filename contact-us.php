<?php
include 'header_inner.php';
?>

<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights" data-animated="fadeInLeftBig" data-animation-delay="400">Контакты</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					
					<li class="typo-dark">Контакты</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- PAGE HEADING SECTION -->
<!-- ADDRESS SECTION -->
<section id="contact" class="no-margin no-padding light-bg">
	<div class="row theme-bg">
		<div class="col-sm-8">
			<!-- MAP SECTION -->
			<!--MAP-->
			<div style="height: 350px;" class="map-canvas" data-zoom="12" data-lat="55.763807" data-lng="37.634660" data-title="Atlas" data-type="roadmap" data-hue="" data-content="Atlas&lt;br&gt; Contact: 8 800 100-43-65&lt;br&gt; info@atlasreg.com"></div>
			<!-- MAP SECTION -->
		</div>
		<div class="col-md-4 col-sm-3 top-margin-md">
			<div class="typo-light section-title parallax-title-1">
				<h2>Адрес</h2>
			</div>
			<div class="typo-light top-margin-sm">
				<ul class="list-unstyled">
					<li>г. Москва, </li>
					<li>Мясницкая 17, </li>
					<li>стр. 2 </li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row light-bg top-margin-md">
			<div class="col-md-6 col-sm-6">
				<div class="feature-box style-3 ">
					<div class="icon-wrapper">
						<!-- ICON -->
						<i class="fa fa-envelope-o fa-2x "></i>
					</div>
					<div class="inner-wrapper">
						<div class="title-wrapper">
							<!-- FEATURE TITLE -->
							<h6 class="feature-title">У вас есть вопросы?</h6>
						</div>
						<!-- FEATURE DESCRIPTION -->
						<p class="feature-description">Почта info@atlasreg.com</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="feature-box style-3 ">
					<div class="icon-wrapper">
						<!-- ICON -->
						<i class="fa fa-mobile fa-3x "></i>
					</div>
					<div class="inner-wrapper">
						<div class="title-wrapper">
							<!-- FEATURE TITLE -->
							<h6 class="feature-title">звоните</h6>
						</div>
						<!-- FEATURE DESCRIPTION -->
						<p class="feature-description">8 800 100-43-65</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="contacts" class="bottom-padding-md">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center bottom-margin-md">
				<!-- SECTION TITLE -->
				<div class="parallax-title no-padding">
					<h2>СВЯЗАТЬСЯ</h2>
				</div>
			</div>
			<div class="col-md-12 bottom-margin-sm">
				<p class="form-message" style="display: none;"></p>
				<div class="contact-form">
					<!-- Form Begins -->
					<form name="bootstrap-form" id="bootstrap-form" method="post" action="php/contact-form.php" novalidate="novalidate" class="bv-form">
						<button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
						<div class="inline-form">
							<div class="row">
								<!-- Field 1 -->
								<div class="col-md-6 input-text form-group has-feedback">
									<input name="contact_name" class="input-name form-control" placeholder="Имя" data-bv-field="contact_name" type="text"><i style="display: none;" class="form-control-feedback bv-no-label" data-bv-icon-for="contact_name"></i>
									<small style="display: none;" class="help-block" data-bv-validator="notEmpty" data-bv-for="contact_name" data-bv-result="NOT_VALIDATED">Please enter a value</small>
								</div>
								<!-- Field 2 -->
								<div class="col-md-6 input-email form-group has-feedback">
									<input name="contact_email" class="input-email form-control" placeholder="Эл. адрес" data-bv-field="contact_email" type="text"><i style="display: none;" class="form-control-feedback bv-no-label" data-bv-icon-for="contact_email"></i>
									<small style="display: none;" class="help-block" data-bv-validator="notEmpty" data-bv-for="contact_email" data-bv-result="NOT_VALIDATED">Please enter a value</small><small style="display: none;" class="help-block" data-bv-validator="emailAddress" data-bv-for="contact_email" data-bv-result="NOT_VALIDATED">Please enter a valid email address</small>
								</div>
							</div>
						</div>
						<!-- Button -->
						<div class="inline-form">
							<div class="row">
								<!-- Field 3 -->
								<div class="col-md-6 input-text form-group has-feedback">
									<input name="contact_phone" class="input-phone form-control" placeholder="Телефон *" data-bv-field="contact_phone" type="text"><i style="display: none;" class="form-control-feedback bv-no-label" data-bv-icon-for="contact_name"></i>
									<small style="display: none;" class="help-block" data-bv-validator="notEmpty" data-bv-for="contact_name" data-bv-result="NOT_VALIDATED">Please enter a value</small>
								</div>
								<!-- Field 4 -->
								<div class="col-md-6 input-email form-group has-feedback">
									<input name="contact_name" class="input-name form-control" placeholder="Тема" data-bv-field="contact_name" type="text"><i style="display: none;" class="form-control-feedback bv-no-label" data-bv-icon-for="contact_email"></i>
									<small style="display: none;" class="help-block" data-bv-validator="notEmpty" data-bv-for="contact_email" data-bv-result="NOT_VALIDATED">Please enter a value</small><small style="display: none;" class="help-block" data-bv-validator="emailAddress" data-bv-for="contact_email" data-bv-result="NOT_VALIDATED">Please enter a valid email address</small>
								</div>
							</div>
						</div>
						<div class="textarea-message form-group has-feedback">
							<textarea name="contact_message" class="textarea-message form-control" placeholder="Сообщение *" rows="4" data-bv-field="contact_message"></textarea><i style="display: none;" class="form-control-feedback bv-no-label" data-bv-icon-for="contact_message" data-original-title="" title=""></i>
							<small style="display: none;" class="help-block" data-bv-validator="notEmpty" data-bv-for="contact_message" data-bv-result="NOT_VALIDATED">Please enter a value</small></div>
						<!-- Button -->
						<button class="btn btn-md btn-theme btn-square btn-hv-dark btn-hv-dark btn-default" type="submit">ОТПРАВИТЬ</button>
					</form>
					<!-- Form Ends -->
				</div>
			</div>

		</div>
	</div>
</section>

<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>