<?php
include 'header_inner.php';
?>
		
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights" data-animated="fadeInLeftBig" data-animation-delay="400">Услуги</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li class="typo-dark">Услуги</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- PAGE HEADING SECTION -->
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
	
	<div class="row">
			<div class="col-md-12">
				<!-- POST -->
				<div class="row">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="#">
							
							</a></h4>
						</div>
						<!-- IMAGE -->
						<div class="post-image">
							<a href="#"><img class="img-responsive" src="images/finance_1300x200.jpg" alt="" /></a>
							
						</div>
						<!-- CONTENT -->
						
					   
					</div>
				</div>
			  
			   
					</div>
				</div>
				<!-- POST -->
	
	<div class="row list-post">
		<div class="col-md-12 top-margin-md">
		
		
		<p class="text-justify">
		Компания Atlasreg предлагает индивидуальные решения для Вас, Вашей семьи и Вашего бизнеса. Все наши клиенты, как частные лица, так и корпоративные клиенты, ценят неизменно высокое качество предоставляемых нами услуг. Наша цель – обеспечить в долгосрочной перспективе Ваше финансовое благополучие. Весь спектр оказываемых нами услуг, будь то открытие иностранной оффшорной компании, открытие банковского счета или предоставление вида на жительства, нацелен на защиту интересов наших клиентов, обеспечивая им не только финансовую самостоятельность, но и независимость от различных обстоятельств и конъюнктуры внешней среды.
		</p>
		</div>
	</div>
		
			</div>
			
		</div>
	</div>
</section>

<!--section blocks-->
<section id="why-choose-us" class="theme-bg border-custom services-blocks">
	<div class="container">
		<div class="row">
			<div class=" col-md-4 col-sm-4 bottom-padding-sm animated fadeInLeft visible block" data-animation="fadeInLeft">
				<!-- FEATURE BOX -->
				<div class="feature-box box_custom  hv-wrapper style-3">
					<div class="feature-inner-wrapper">
						<div class="icon-wrapper">
							<!-- ICON -->
							<i class="fa fa-briefcase fa-2x icon-shape-circle icon-dark icon-grey-bg icon-hvtheme icon-hvtransparent-bg icon-hvbordered-theme border b2 solid"></i>
						</div>
						<div class="title-wrapper typo-light">
							<!--TITLE -->
							<h5 class="text-uppercase feature-title"> Регистрация иностранных компаний  </h5>
							<!-- TEXT -->
							<p>
							Используйте сервис в качестве инструмента налогового планирования или налоговой оптимизации
							
							</p>
							<a href="ourstrategy.php">Читать далее ...</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 bottom-padding-sm animated fadeInLeft visible block" data-animation="fadeInLeft">
				<!-- FEATURE BOX -->
				<div class="feature-box box_custom  hv-wrapper style-3">
					<div class="feature-inner-wrapper">
						<div class="icon-wrapper">
							<!-- ICON -->
							<i class="fa fa-newspaper-o fa-2x icon-shape-circle icon-dark icon-grey-bg icon-hvtheme icon-hvtransparent-bg icon-hvbordered-theme border b2 solid icon-shape"></i>
						</div>
						<div class="title-wrapper typo-light">
							<!--TITLE -->
							<h5 class="text-uppercase feature-title">Открытие банковских счетов  </h5>
							<!-- TEXT -->
							<p>
							Откройте свой личный или корпоративный банковский счет в лучших банках мира

							</p>
							<a href="open_account.php">Читать далее ...</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 bottom-padding-sm animated fadeInLeft visible block" data-animation="fadeInLeft">
				<!-- FEATURE BOX -->
				<div class="feature-box box_custom  hv-wrapper style-3">
					<div class="feature-inner-wrapper">
						<div class="icon-wrapper">
							<!-- ICON -->
							<i class="fa fa-pie-chart fa-2x icon-shape-circle icon-dark icon-grey-bg icon-hvtheme icon-hvtransparent-bg icon-hvbordered-theme border b2 solid icon-shape"></i>
						</div>
						<div class="title-wrapper typo-light">
							<!--TITLE -->
							<h5 class="text-uppercase feature-title">Получение вида на жительства</h5>
							<!-- TEXT -->
							<p>
Ключ к свободе передвижения, защите активов и конфиденциальности, расширению возможностей инвестирования и снижению налогов.

							</p>
							<a href="residence.php">Читать далее ...</a>
						</div>
					</div>
				</div>
			</div>
		   
		</div>
	</div>
</section>
<!-- /.GRID SECTION -->

<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>