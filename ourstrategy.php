<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Регистрация иностранных компаний</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li class="typo-dark">Регистрация иностранных компаний</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar ">
	<div class="container">
		<div class="row bottom-margin-md" data-animation="fadeInUp" data-animation-delay="400">
			
		</div>
		<div class="row">
			<div class="col-md-9">
				<!-- COLUMN 1/2 -->
				<div class="row bottom-margin-md" data-animation="fadeInUp" data-animation-delay="400">
					
					<div class="col-sm-12">

						
						<p class="text-justify">
						Одним из основных направлений деятельности Атласрег является регистрация иностранных компаний.
Мы занимаемся регистрацией компаний в классических оффшорах, Европе, Азии, Центральной Америке и во многих других юрисдикциях.

						</p>
					</div>
				</div>
				<!-- /.COLUMN 1/2 -->
				<!-- PIE CHARTS STYLES -->
			</div>
			<div class="col-md-3 sidebar no-padding">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="classical.php"  class="list-group-item">Классические оффшорные юрисдикции</a>
							<a href="low_tax.php" class="list-group-item">Низконалоговые юрисдикции</a>                                       
							<a href="europian.php" class="list-group-item">Европейские юрисдикции</a> 
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
</section>
<section id="about-us" class="bottom-padding-md padding-top-20">
	<div class="container">
		
		<div class="row bottom-padding-md">
			<div class="col-md-9 col-sm-12">
				<div class="section-title no-margin">
					<!-- TITLE -->
					<h3 class="parallax-title-1">
					<span class="text-style"> 
					Классификация иностранных компаний
					</span> </br>
					</h3>
					<!-- TEXT -->
					<p>
					С точки зрения налоговой нагрузки и общего правового регулирования, иностранные компании можно условно разделить на три категории:
					</p>
					<ul>
						<li><a href="classical.php">Классические оффшорные юрисдикции</a></li>
						<li><a href="low_tax.php">Низконалоговые юрисдикции</a></li>
						<li><a href="europian.php">Европейские юрисдикции</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row bottom-padding-md">
			<div class="col-md-9 col-sm-12">
				<div class="section-title no-margin">
					<!-- TITLE -->
					<h3 class="parallax-title-1">
					<span class="text-style"> 
					Преимущества использования иностранной компании
					</span> </br>
					</h3>
					<!-- TEXT -->  
					<ul>
						<li>Существенная налоговая экономия или полное отсутствие налогообложения</li>
						<li>Отсутствие валютного контроля</li>
						<li>Минимальные требования к финансовой отчетности, бухгалтерскому учету и аудиту (или их отсутствие)</li>
						<li>Уменьшение юридических рисков</li>
						<li>Защита персональной информации</li>
						<li>Безопасность и анонимность владения активами</li>
						<li>Защита активов от рейдеров, нестабильной политической обстановки, судебных решений</li>
						<li>Ведение бизнеса в качестве полноправного субъекта на международном рынке</li>
						<li>Расширение географии бизнеса</li>
						<li>Нахождение капитала за рубежом</li>
						<li>Мгновенная регистрация компании</li>
					</ul>	
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title no-margin">
					<!-- TITLE -->
					<h3 class="parallax-title-1">
					<span class="text-style"> 
					Процесс и этапы регистрации иностранной компании
					</span> </br>
					</h3>
					<!-- TEXT -->  
					<ul>
						<li>Выбор компании (определение юрисдикции, организационно-правовой формы)</li>
						<li>Предоставление документов</li>
						<li>Согласование наименования компании</li>
						<li>Оплата услуг по регистрации компании</li>
						<li>Оформление компании (подача документов в регистрирующие органы)</li>
						<li>Получение документов</li>
					</ul>						
				</div>
			</div>
		</div>
		
		</div>
	</div>
</section>

<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>