<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Ирландия</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Ирландия</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a"><img class="img-responsive" width="960" height="640" src="images/flags/ireland.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Ирландия
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="european_hungary.php"  class="list-group-item">Венгрия</a>
							<a href="european_ireland.php" class="list-group-item active">Ирландия</a>                                       
							<a href="#" class="list-group-item">Лихтенштейн</a>
							<a href="#" class="list-group-item">Мальта</a>
							<a href="#" class="list-group-item">Нидерланды</a>
							<a href="european_estonia.php" class="list-group-item">Эстония</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании в Ирландии	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Стандартный пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					<tr>
						  <td>Проверка названия</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Регистрация компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оплата 	госпошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление уставных документов</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Предоставление юридического адреса на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга агента на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Доставка документов курьерской службой</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление доп. комплекта документов под апостилем</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального директора на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального акционера на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					 
					  <tr>
						  <td>Общая стоимость</td>
						  <td>от €1900</td>
						  <td>от €2100</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>от €1400</td>
						  <td>от €1600</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Основные особенности компании в Ирландии	
				</span>								
				</h3>
				<p>
				
				<table class="table no-border text-center">
					
					<tbody>
						<tr>
						  <td>Организационно-правовая форма</td>
						  <td>Private Limited Liability Company</td>
					  </tr>
					  <tr>
						  <td>Система права в стране</td>
						  <td>Английское общее право</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность реестра</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Раскрытие данных о бенефициаре</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования к акционерам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество акционеров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности акционеров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускаются акционеры-юридические лица</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к директорам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество директоров</td>
						  <td>2</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности директоров</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Допускается ли директор-юридическое лицо</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования к секретарю</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требование по наличию секретаря</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли секретарь-юридическое лицо</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Уставный капитал и акции</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальный размер объявленного капитала</td>
						  <td>Нет ограничений</td>
					  </tr>
					  <tr>
						  <td>Минимальный размер оплаченного капитала</td>
						  <td>Обычно 1 EUR</td>
					  </tr>
					  <tr>
						  <td>Срок оплаты объявленного капитала</td>
						  <td>Нет ограничений</td>
					  </tr>
					  <tr>
						  <td>Выпуск акций на предъявителя</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Налогообложение в Ирландии</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Налог на прибыль</td>
						  <td>12,5% (торговая деятельность), 25% (неторговая деятельность)</td>
					  </tr>
					  <tr>
						  <td>НДС</td>
						  <td>23%</td>
					  </tr>
					  <tr>
						  <td>Налог у источника (дивиденды, проценты)</td>
						  <td>20% (роялти по патенту - 20%, остальное - 0%) </td>
					  </tr>
					  <tr>
						  <td>Налог на прирост капитала</td>
						  <td>33%, 40%</td>
					  </tr>
					  <tr>
						  <td>Дополнительные обязательные местные налоги</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Валютный контроль</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования по отчетности</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требования по сдаче отчетности</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требование по подаче Annual Return</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Открытый доступ к отчетности</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Дополнительная информация</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Членство в ОЭСР (OECD)</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Количество подписанных Соглашений об избежании двойного налогообложения</td>
						  <td>68</td>
					  </tr>
					  <tr>
						  <td>Наличие Соглашения об избежании двойного налогообложения с Россией</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Группа риска: ЦБ РФ N 1317-У</td>
						  <td>Да (группа 1</td>
					  </tr>
					  <tr>
						  <td>Группа риска: Минфин РФ Приказ N 108н</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации новой компании</td>
						  <td>3 недели</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Особенности регистрации компании в Ирландии	
				</span>								
				</h3>
				<ul>
					<li>Количество акций ограничивает ответственность участников компании;</li>
					<li>В названии должно быть слово Teoranta (Teo) или Limited (Ltd), что указывает на форму компании. Для включения в название таких слов как "standard", "bank", "insurance", "society", "group", "holdings", "international" и пр. необходимо получить специальное разрешение;</li>
					<li>Компания может начинать деятельность при наличии хотя бы 1 директора. Корпоративные директора не разрешены. Хотя бы один из директоров обязан быть резидентом страны ЕЭЗ. Если данное требование не выполняется, компании необходимо приобрести ценную бумагу (номинал – € 25 395), которая выпущена ирландским банком, сроком – от двух лет. Не более 25 ирландских компаний могут быть одновременно возглавлены ирландским резидентом;</li>
					<li>Директор может являться и секретарем компании. В функциях секретаря – ведение протоколов собраний, реестра, содержащего информацию о:
						<ul>
							<li>директорах и секретарях;</li>
							<li>акционерах;</li>
							<li>доходах и расходах.</li>
						</ul>
					</li>
					<li>Минимальный выпущенный капитал не фиксирован;</li>
					<li>Размер минимального оплаченного капитала – 1% от суммы выпущенного;</li>
					<li>В компании должно быть не более 50 акционеров;</li>
					<li>Акции нельзя размещать на бирже;</li>
					<li>Возможен выпуск привилегированных и обыкновенных акций;</li>
					<li>Офис компании должен быть зарегистрирован на территории юрисдикции.</li>
				</ul>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>