<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Панама</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Панама</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a><img class="img-responsive" width="960" height="640" src="images/flags/Panama.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Панама
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="offshore_seishel.php"  class="list-group-item">Сейшельские Острова</a>
							<a href="offshore_bahamas.php" class="list-group-item">Багамские Острова</a>                                       
							<a href="offshore_virgins.php" class="list-group-item">Британские Виргинские Острова</a>
							<a href="offshore_kaiman.php" class="list-group-item">Каймановы Острова</a>
							<a href="offshore_beliz.php" class="list-group-item">Белиз</a>
							<a href="offshore_panama.php" class="list-group-item active">Панама</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании в Панаме	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Базовый пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					  <tr>
						  <td>Проверка названия</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Регистрация компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оплата госпошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление уставных документов</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Предоставление юридического адреса на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга агента на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Доставка документов курьерской службой</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление доп. комплекта документов под апостилем</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального директора на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального акционера на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Общая стоимость</td>
						  <td>1000$</td>
						  <td>1400$</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>1050$</td>
						  <td>1550$</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
				
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Основные особенности оффшорной компании в Панаме
				</span>								
				</h3>
				<p>
				
				<table class="table one-third">
					<thead>
					  <tr>
						<th>Параметры</th>
						<th>Значения</th>										
					  </tr>
					</thead>
					<tbody>
					  <tr>
						  <td>Тип компании</td>
						  <td>Corporation</td>
					  </tr>
					  <tr>
						  <td>Название</td>
						  <td>Должно оканчиваться словами  "Corporation", "Corp.", "Incorporated", "Inc"</td>
					  </tr>
					  <tr>
						  <td>Корпоративное законодательство</td>
						  <td>GENERAL CORPORATION LAW, 1927</td>
					  </tr>
					 
					  <tr>
						  <td>Мин. количество акционеров</td>
						  <td>1 (может быть юр. лицом)</td>
					  </tr>
					  <tr>
						  <td>Номинальные акционеры</td>
						  <td>Разрешены</td>
					  </tr>
					  <tr>
						  <td>Мин. количество директоров</td>
						  <td>3</td>
					  </tr>
					  <tr>
						  <td>Номинальные директора</td>
						  <td>Разрешены</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность</td>
						  <td>Высокая. В стране закрытый регистр акционеров и директоров. Информация предоставляется только по решению суда. При использовании номинальных директоров и акционеров истинные владельцы компании вообще не могут быть установлены.</td>
					  </tr>
					  <tr>
						  <td>Проведение ежегодных собраний</td>
						  <td>Не требуется. При необходимости могут быть проведены в любой стране мира (возможно по телефону).</td>
					  </tr>
					  <tr>
						  <td>Сдача ежегодной отчетности</td>
						  <td>Не требуется</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Не требуется</td>
					  </tr>
					  <tr>
						  <td>Налогообложение</td>
						  <td>Отсутствует</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации и доставки документов</td>
						  <td>2 недели</td>
					  </tr>
					  <tr>
						  <td>Приобретение готовой компании</td>
						  <td>Возможно</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Особенности регистрации компании в Панаме
				</span>	
				<span class="text-style">
					Наименование
				</span>								
				</h3>
				<p class="bottom-padding-md">
				Компания в Панаме должна носить уникальное название. В нём не может содержаться таких слов как Bank, Foundations, Trust, Insurance, Finance. Зарегистрированное предприятие не имеет права заниматься фондовой или трастовой, банковской или страховой деятельностью, предоставлять свой юридический адрес для пользования другими компаниями без соответствующего разрешения Министерства промышленности и торговли Панамы.
После предоставления названия компании регистратору проводится специальная проверка на наличие аналогичных наименований. Если таковых нет, осуществляются дальнейшие шаги по оформлению.

				</p>
				
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Согласование структуры
				</span>								
				</h3>	
				<p class="bottom-padding-md">
				Этот процесс включает в себя определение акционера, директора, принципа распределения акций. Принимается решение о том, будет ли использоваться номинальный сервис.
				</p>
				
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Базовый комплект документов оффшорной компании в Панаме 
				</span>								
				</h3>
				<ul class="bottom-padding-md">
					<li>Articles of incorporation (Свидетельство о регистрации)</li>
					<li>Apostilled Extract from the Register (Апостилированная Выписка из Реестра)</li>
					<li>Share Certificates (Сертификат акций)</li>
					<li>Minutes of the Meeting of the Board of the Directors (Протокол Собрания Директоров (Выпуск Акций))</li>
					<li>Apostilled Minutes of the Meeting of the Board of the Directors (Апостилированный Протокол Собрания Директоров (Выпуск Доверенности))</li>
					<li>Agreement for the provision of nominee services (Договор на предоставление номинальных услуг)</li>
					<li>Page with Apostille (Апостиль на документах (по запросу))</li>
				</ul>
			</div>
		</div>
	</div>
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>