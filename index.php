<?php
include 'header.php';
?>

<!-- Slider -->
<section class="parallax-bg bg-fixed bg-image slider-section single-screen padding-bottom-20 bg-no-repeat typo-light" data-background="images/content/slider/43.jpg">
	<!-- ITEM  -->
	<div class="top-padding">
		<div class="container slider-content text-center">
			<!-- TITLE  -->
			<h4>Комплексные решения для</h4>
			<h1 style="text-align: center;font-family:Raleway;font-weight:800;font-style:normal">
			Вас и Вашего Бизнеса
			</h1>
			<div class="top-margin bottom-padding-sm slider-title">
				<!-- TEXT  -->
				<!--<h4>Мы давно работаем в сфере международных консалтинговых услуг 
				<br>и готовы помочь вам открыть счет в иностранном банке, который будет максимально соответствовать вашим целям и ожиданиям.
				</h4>-->
				<!-- BUTTON  -->
			</div>
			
		</div>
	</div>
	<!--data count-->
	<div class="container text-left">
		<div class="row">     
			<div class="col-md-10 col-md-offset-1 text-center">
				<!-- TEXT -->
				<p>
				Более 15 лет мы консультируем частных лиц и корпоративных клиентов из России, СНГ и мира по вопросам открытия и сопровождения иностранных компаний, банковских счетов, налогового планирования и защиты активов.
				</p>
			</div>
		</div>
		<div class="row top-padding-sm">
			<div class="col-md-4 col-xs-6">
				<div class="text-center counter-wrapper hv-wrapper ">
					<!-- COUNTER NUMBER -->
					<div data-count="15" class="number-counter "><span class="counter"></span></div>
					<!-- COUNTER TITLE -->
					<h5 class="text-grey">лет
					
					</h5>
				</div>
			</div>
			<div class="col-md-4 col-xs-6">
				<div class="text-center counter-wrapper hv-wrapper">
					<!-- COUNTER NUMBER -->
					<div data-count="45" class="number-counter"><span class="counter"></span></div>
					<!-- COUNTER TITLE -->
					<h5 class="text-grey">юрисдикций </h5>
				</div>
			</div>
			<div class="col-md-4 col-xs-6">
				<div class="text-center counter-wrapper hv-wrapper">
					<!-- COUNTER NUMBER -->
					<div data-count="120" class="number-counter"><span class="counter"></span></div>
					<!-- COUNTER TITLE -->
					<h5 class="text-grey">реализованных проектов</h5>
				</div>
			</div>
		</div>
	</div>

</section>			
<section id="about-us" class="bottom-padding-md">
	<div class="container">
		<div class="row">
			<div class="col-md-11 text-center">
				<!-- SECTION TITLE -->
				<div class="section-title">
					<h3 class="parallax-title">Регистрация иностранных компаний, открытие банковских счетов, получение вида на жительство вместе с Атласрег</h3>
					<div class="bottom-pad padding-bottom-20">
						Индивидуальный подход к каждому клиенту – это один из главных принципов работы нашей компании. Мы готовы поделиться с нашими клиентами информацией о всех преимуществах, деталях, нюансах регистрации, владения и деятельности иностранных компаний, открытия личных или корпоративных банковских счетов или получения видов на жительства.  Атласрег берёт на себя всю сложную работу, а Вы получаете видимый результат в простой форме.
					</div>
					<a class="btn btn-outline btn-hv-dark btn-md btn-rounded" href="our-business.php">О компании</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="services2" class="no-bottom-padding services-blocks padding-top-10">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<!-- SECTION TITLE -->
				<div class="section-title">
					<h3 class="parallax-title">Услуги</h3>
					<div class="bottom-pad">
						<p>
						На сегодня услуга открытия счетов в иностранных банках пользуется большой популярностью среди компаний и частных лиц.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row block">
			<div class="col-md-4 col-sm-6 bottom-margin-sm box_custom">
				<!-- FEATURE BOX -->
				<div class="feature-box">
					<div class="icon-wrapper">
						<!-- ICON -->
						<i class="fa fa-briefcase fa-2x "></i>
					</div>
					<div class="inner-wrapper">
						<div class="title-wrapper">
							<!-- FEATURE TITLE -->
							<h5 class="feature-title">Регистрация иностранных компаний</h5>
						</div>
						<!-- FEATURE DESCRIPTION -->
						<p class="feature-description">
						Используйте сервис в качестве инструмента налогового планирования или налоговой оптимизации
						</p>
						<a href="ourstrategy.php">Читать далее ...</a>
					</div>
				</div>
				<!-- /.FEATURE BOX -->
			</div>
			
			<div class="col-md-4 col-sm-6 bottom-margin-sm box_custom">
				<!-- FEATURE BOX -->
				<div class="feature-box">
					<div class="icon-wrapper">
						<!-- ICON -->
						<i class="fa fa-calculator fa-2x"></i>
					</div>
					<div class="inner-wrapper">
						<div class="title-wrapper">
							<!-- FEATURE TITLE -->
							<h5 class="feature-title">Открытие банковских счетов</h5>
						</div>
						<!-- FEATURE DESCRIPTION -->
						<p class="feature-description">
						Откройте свой личный или корпоративный банковский счет в лучших банках мира
						</p>
						<a href="open_account.php">Читать далее ...</a>
					</div>
				</div>
				<!-- /.FEATURE BOX -->
			</div>
			<div class="col-md-4 col-sm-6 bottom-margin-sm box_custom">
				<!-- FEATURE BOX -->
				<div class="feature-box">
					<div class="icon-wrapper">
						<!-- ICON -->
						<i class="fa fa-bar-chart fa-2x"></i>
					</div>
					<div class="inner-wrapper">
						<div class="title-wrapper">
							<!-- FEATURE TITLE -->
							<h5 class="feature-title">Получение вида на жительства</h5>
						</div>
						<!-- FEATURE DESCRIPTION -->
						<p class="feature-description">
						Ключ к свободе передвижения, защите активов и конфиденциальности, расширению возможностей инвестирования и снижению налогов.

						</p>
						<a href="residence.php">Читать далее ...</a>
					</div>
				</div>
				<!-- /.FEATURE BOX -->
			</div>
		   
		</div>
	</div>
</section>

<?php
include 'footer.php';
?>