<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Каймановы острова</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Каймановы острова</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a><img class="img-responsive" width="960" height="640" src="images/flags/KaimanIsland.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Каймановы острова
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="offshore_seishel.php"  class="list-group-item">Сейшельские Острова</a>
							<a href="offshore_bahamas.php" class="list-group-item">Багамские Острова</a>                                       
							<a href="offshore_virgins.php" class="list-group-item">Британские Виргинские Острова</a>
							<a href="offshore_kaiman.php" class="list-group-item active">Каймановы Острова</a>
							<a href="offshore_beliz.php" class="list-group-item">Белиз</a>
							<a href="offshore_panama.php" class="list-group-item">Панама</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании на Каймановых островах	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Базовый пакет</th>
						<th>Стандартный пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					  <tr>
						  <td>Проверка названия</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Регистрация компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оплата госпошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление уставных документов</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Предоставление юридического адреса на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга агента на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Доставка документов курьерской службой</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление доп. комплекта документов под апостилем</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального директора на один год</td>
						  <td></td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального акционера на один год</td>
						  <td></td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Общая стоимость</td>
						  <td>3600$</td>
						  <td>4000$</td>
						  <td>5300$</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>от 2900$</td>
						  <td>от 3500$</td>
						  <td>от 4600$</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
				
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Основные особенности оффшорной компании  на Каймановых островах.
				</span>								
				</h3>
				<p>
				
				<table class="table one-third">
					<thead>
					  <tr>
						<th>Параметры</th>
						<th>Значения</th>										
					  </tr>
					</thead>
					<tbody>
					  <tr>
						  <td>Организационно-правовая форма</td>
						  <td>IBC (International Business Company)</td>
					  </tr>
					  <tr>
						  <td>Система права в стране</td>
						  <td>Английское право</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность реестра</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Раскрытие данных о бенефициаре</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Требования к акционерам</td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Мин. количество акционеров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности акционеров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускаются акционеры-юридические лица</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требования к директорам</td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество директоров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности директоров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли директор-юридическое лицо</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требования к секретарю</td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требование по наличию секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли секретарь-юридическое лицо</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Уставный капитал и акции</td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальный размер объявленного капитала</td>
						  <td>Нет (обычно 50,000 USD)</td>
					  </tr>
					  <tr>
						  <td>Минимальный размер оплаченного капитала</td>
						  <td>0</td>
					  </tr>
					  <tr>
						  <td>Срок оплаты объявленного капитала</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Выпуск акций на предъявителя</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Налогообложение на Каймановых островах</td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Налог на прибыль</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>НДС</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Налог у источника (дивиденды, проценты)</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Налог на прирост капитала</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Дополнительные обязательные местные налоги</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Валютный контроль</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Требования по отчетности</td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требования по сдаче отчетности</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Требование по подаче Annual Return</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Открытый доступ к отчетности</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Дополнительная информация</td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Членство в ОЭСР (OECD)</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Количество подписанных Соглашений об избежании двойного налогообложения</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Наличие Соглашения об избежании двойного налогообложения с Россией</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Группа риска: ЦБ РФ N 1317-У</td>
						  <td>Да (группа 2)</td>
					  </tr>
					  <tr>
						  <td>Группа риска: Минфин РФ Приказ N 108н</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации новой компании</td>
						  <td>2-3 недели</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Особенности оффшорной компании на Каймановых Островах
				</span>								
				</h3>
				
				<ul class="bottom-padding-md">
					<li>Разрешена любая законная деятельность. Специальному лицензированию подлежит банковская, страховая или перестраховочная, трастовая сфера;</li>
					<li>Запрещено:
						<ul>
							<li>владение недвижимостью на землях юрисдикции,</li>
							<li>ведение коммерции с резидентами,</li>
							<li>привлечение компаний, являющихся резидентами, к менеджменту оффшора на Каймановых Островах.</li>
						</ul>				
					</li>
					<li>Регистрация компании происходит в течение 48 часов после предоставления всех требуемых документов;</li>
					<li>Название компании должно быть уникальным и не может быть идентичным либо похожим на уже существующие названия. Регистрация названия может быть произведена на любом языке, но перевод на английский язык – обязательны;</li>
					<li>Регистрация оффшорной компании на Каймановых Островах происходит при наличии следующих документов:
						<ul>
							<li>Устава;</li>
							<li>Меморандума об учреждении.</li>
						</ul>
					</li>
					<li>Адрес зарегистрированной фирмы обязательно должен находиться на территории Каймановых Островов. На него будет приходить корреспонденция из государственных органов управления;</li>
					<li>Требований об обязательной уплате уставного капитала нет;</li>
					<li>Выпуск именных акций и акций на предъявителя, со стоимостью в любой валюте, разрешен;</li>
					<li>Резиденты любой страны мира, юридические и физические лица могут стать акционерами оффшора на Каймановых Островах. </li>
					<li>Резиденты любой страны мира, юридические и физические лица могут стать директорами компании. Оффшор может управляться одним директором, наделенным всеми правами, за исключением тех, что принадлежат акционерами;</li>
					<li>В любом уголке мира могут проводиться собрания акционеров, в том числе – и по телефону. Хранение протоколов собраний разрешено в любом месте, указанном в документах об учреждении;</li>
					<li>Минимум раз в год должно быть проведено собрание директоров компании;</li>
					<li>Учредительные, финансовые документы могут храниться в любом государстве мира;</li>
					<li>Гарантируется конфиденциальность данных об акционерах и директорах компании на Каймановых островах, содержащихся в учредительной документации. Доступ к информации может быть разрешен только после соответствующего решения судебного органа, если доказана причастность компании к поддержке террористических организаций, продаже наркотиков, незаконной продаже оружия, отмывания доходов.</li>
				</ul>
				
				
			</div>
		</div>
	</div>
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>