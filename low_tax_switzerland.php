<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Швейцария</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Швейцария</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a"><img class="img-responsive" width="960" height="640" src="images/flags/switzerland.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Швейцария
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="low_tax_cyprus.php"  class="list-group-item">Кипр</a>
							<a href="low_tax_switzerland.php" class="list-group-item active">Швейцария</a>                                       
							<a href="low_tax_england.php" class="list-group-item">Англия (LLC)</a>
							<a href="low_tax_scotland.php" class="list-group-item">Шотландия (LLC)</a>
							<a href="low_tax_jerci.php" class="list-group-item">Остров Джерси</a>
							<a href="low_tax_honkong.php" class="list-group-item">Гонконг</a>
							<a href="low_tax_singapore.php" class="list-group-item">Сингапур</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании в Швейцарии	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Компания GMBH</th>
						<th>Компания AG</th>									
					  </tr>
					</thead>
					<tbody>
					<tr>
						  <td>Регистрация компании, включая государственные пошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Апостилированный пакет</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Юридический адрес на 1 год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуги директора компании (Резидент Швейцарии)</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Справка о резидентности компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Общая стоимость</td>
						  <td>9 900 EUR</td>
						  <td>12 900 EUR</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>8 900 EUR</td>
						  <td>8 900 EUR</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Основные особенности компании в Швейцарии	
				</span>								
				</h3>
				<p>
				
				<table class="table no-border text-center">
					
					<tbody>
						<tr>
						  <td>Организационно-правовая форма</td>
						  <td>SARL (Societe a Responsibilite Limite)</td>
					  </tr>
					  <tr>
						  <td>Система права в стране</td>
						  <td>Гражданское право</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность реестра</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Раскрытие данных о бенефициаре</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к акционерам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество акционеров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности акционеров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускаются акционеры-юридические лица</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к директорам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество директоров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности директоров</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Допускается ли директор-юридическое лицо</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования к секретарю</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требование по наличию секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли секретарь-юридическое лицо</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Уставный капитал и акции</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальный размер объявленного капитала</td>
						  <td>20,000 CHF</td>
					  </tr>
					  <tr>
						  <td>Минимальный размер оплаченного капитала</td>
						  <td>10,000 CHF</td>
					  </tr>
					  <tr>
						  <td>Срок оплаты объявленного капитала</td>
						  <td>Не установлен</td>
					  </tr>
					  <tr>
						  <td>Выпуск акций на предъявителя</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Налогообложение в Швейцарии</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Налог на прибыль</td>
						  <td>15,64% (Цуг)</td>
					  </tr>
					  <tr>
						  <td>НДС</td>
						  <td>8%</td>
					  </tr>
					  <tr>
						  <td>Налог у источника (дивиденды, проценты)</td>
						  <td>0-35%</td>
					  </tr>
					  <tr>
						  <td>Налог на прирост капитала</td>
						  <td>0%</td>
					  </tr>
					  <tr>
						  <td>Дополнительные обязательные местные налоги</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Валютный контроль</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования по отчетности</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требования по сдаче отчетности</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требование по подаче Annual Return</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Открытый доступ к отчетности</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Дополнительная информация</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Членство в ОЭСР (OECD)</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Количество подписанных Соглашений об избежании двойного налогообложения</td>
						  <td>99</td>
					  </tr>
					  <tr>
						  <td>Наличие Соглашения об избежании двойного налогообложения с Россией</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Группа риска: ЦБ РФ N 1317-У</td>
						  <td>Да (группа 1)</td>
					  </tr>
					  <tr>
						  <td>Группа риска: Минфин РФ Приказ N 108н</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации новой компании</td>
						  <td>3-4 недели</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Формы ведения бизнеса в Швейцарии
				</span>								
				</h3>
				<p>
				Два основных вида компаний в Швейцарии – это:
				</p>
				<ul>
					<li>Открытое Акционерное общество, Aktiengesellschaft (AG);</li>
					<li>Общество с ограниченной ответственностью, Gesellschaft mit baschrankter Haftung (GmbH).</li>	
				</ul>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Процедура регистрации компании в Швейцарии
				</span>								
				</h3>
				<p class="bottom-padding-md">
				До регистрации нужно выбрать название компании (лучше подготовить несколько вариантов наименования). Затем необходимо проверить подготовленное наименование в федеральном учреждении соответствующего направления. Это делается для того, чтобы убедиться в том, что уже зарегистрированной фирмы с аналогичным или подобным названием нет.
Также необходимо подать определенную информацию относительно видов коммерческой деятельности, заниматься которыми будет образуемая компания. Также нужно предоставить данные об учредителях и директоре, на чьё имя запланирована регистрация фирмы в Швейцарии.
После предоставления комплекта требуемых документов регистрация компании занимает примерно неделю (7 рабочих дней). Сроки могут сокращаться или увеличиваться, в зависимости от нагрузки на регистрационный аппарат кантона.
Компания в Швейцарии должна иметь юридический адрес.
Компания может открыть счет в швейцарском банке, что позволяет получить не только квалифицированный сервис в отношении инвестиций, но и гарантии секретности, возможность обслуживания на международных языках. Удобной является и возможность управления своим счетом посредством телефона и Интернета.

				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация общества с ограниченной ответственностью (Gmbh) в Швейцарии
				</span>								
				</h3>
				
				<ul>
					<li>Минимальный капитал составляет 20 000 швейцарских франков. На момент регистрации сумма должна быть полностью внесена;</li>
					<li>Акционерами могут становиться физические и юридические лица с любым гражданством и любой национальности;</li>
					<li>Количество директоров – минимум 1. Это должен быть резидент Швейцарии, в обязанности которого будет входить осуществление контактов с органами государственной власти. Директор также ответственен за выполнение обязательств общества перед регистром и налоговой службой;</li>
					<li>Необходимо ежегодно проводить Собрание акционеров;</li>
					<li>Компания должна иметь швейцарский зарегистрированный офис;</li>
					<li>Органами управления компании являются:
						<ul>
							<li>Общее собрание,</li>
							<li>Дирекция</li>
							<li>Наблюдательный совет (по желанию).</li>
						</ul>
					</li>
					<li>Компания обязана сдавать финансовую отчетность.</li>
				</ul>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>