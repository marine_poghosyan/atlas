<?php
include 'header_inner.php';
?>
	<!-- GRID SECTION -->
	<!-- PAGE HEADING SECTION -->
	<section class="page-header padding-sm page-title-left minimal light-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-left">
					<!-- TITLE-->
					<h2 class="" data-animated="fadeInLeftBig" data-animation-delay="400">О компании</h2>

					<!-- BREADCRUMB-->
					<ul class="breadcrumb">
						<li>
							<a href="./">Главная</a>
						</li>

						<li>О компании</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	
	<!-- INTRO SECTION -->
	<section class="parallax-bg bg-no-repeat no-padding bg-image typo-light" data-stellar-background-ratio="0.5" data-background="images/content/bg/elements/bg-3.jpg">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-12 pull-right">
					<div class="overlay-darker"></div>
					<div class="col-md-12 col-sm-12 padding-md text-left">
						<!-- TITLE  -->
						<h2 class="parallax-title">О компании</h2>
						<p>
							Компания Атласрег предоставляет своим клиентам комплексные сервисы на рынке корпоративных услуг в следующих направлениях:
						</p>
						<!--<label>
							Мы рады предложить Вам наши ключевые услуги:
						</label>-->
						<!-- LIST-->
						<ul >
							<li>Регистрация иностранных компаний;</li>
							<li>Открытие банковских счетов в зарубежных банках;</li>
							<li>Предоставление видов на жительство.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- WHAT WE OFFER SECTION -->
	<section id="offer" class="no-bottom-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12 bottom-margin-md">
					<!-- SECTION TITLE -->
					<div class="parallax-title no-padding">
						<h2></h2>
					</div>
					<p>
					Компания Атласрег имеет партнерские соглашения с крупнейшими международными консалтинговыми компаниями, регистрационными компаниями, банками, аудиторскими организациями и другими профильными структурами.
					</p>
					<p>
					Нами накоплен значительный опыт и разработаны эффективные стандарты работы, которые позволяют максимально оптимизировать процессы взаимодействия между нашей компанией и клиентом.
					</p>
					<p>
					Основной принцип работы компании Атласрег - прежде всего, ориентированность на нужды наших клиентов. 
					</p>
					<p>
					Рекомендации и консультации, основанные на глубоком знании рынка корпоративных услуг, международного бизнес-опыта и превосходном понимании российской действительности, - отличительная черта компании Атласрег. 
					</p>
					<p>
					Именно такое сочетание вкупе с профессиональным подходом наших специалистов позволяет достигать поставленных нашими клиентами целей и задач в кратчайшие сроки. Каждому клиенту, частному лицу или корпоративному клиенту, мы предлагаем сугубо индивидуальное решение.
					</p>
					<p>
					При этом вся наша работа и обширные знания в части предоставляемых нами услуг открыты для наших клиентов. Атласрег берёт на себя всю сложную работу, а Вы получаете видимый результат в простой форме.
					</p>
				</div>
			</div>
		   
		</div>
	</section>
	<!-- WHAT WE OFFER SECTION -->
	
<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>