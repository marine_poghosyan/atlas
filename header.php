<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta charset="utf-8">
    <!-- Latest IE rendering engine & Chrome Frame Meta Tags -->
    <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
        <![endif]-->
    <!-- TITLE-->
    <title>Atlas</title>
    <!-- Favicon -->

    <!-- GOOGLE FONTS
            ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
    <link rel="stylesheet" id="google-fonts-zozo_options-css" href="http://fonts.googleapis.com/css?family=Hind%3A300%2C400%2C500%2C600%2C700%7CRaleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%2C100italic%2C200italic%2C300italic%2C400italic%2C500italic%2C600italic%2C700italic%2C800italic%2C900italic" type="text/css" media="all">
	<link href="https://fonts.googleapis.com/css?family=Oswald:400,600,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=cyrillic" rel="stylesheet">
    <!-- CSS BEGINS 
            ================================================== -->
    <!-- ELEMENTS BASE CSS -->
    <link href="css/elements.min.css" rel="stylesheet" type="text/css" />
    <!-- THEME BASE CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- COLOR SCHEME -->
    <link href="css/colors/color1.css" id="changeable-colors" rel="stylesheet" />
    <!-- CSS ENDS   
		 	 ================================================== -->
	
</head>

<body>
    <!-- Page Loader -->
    <div id="pageloader">
        <div class="loader-inner">
            <img src="images/base/loader.gif" alt="">
        </div>
    </div>
    <!-- PAGE WRAPPER -->
    <div id="page-wrapper" class="header-dropdown-light">
        <div class="content-wrapper">
            <!-- HEADER -->
            <header class="header-details-toggle header-dark header-transparent header-top-transparent header-sticky-dark">
                <!-- STICKY WRAPPER -->
                <div id="sticker" class="header-main sticky-navigation">
                    <nav class="navbar navbar-default fixed">
                        <div class="container">
                            <!-- TOGGLE ICON -->
                            <div class="navbar-header">
                                <a class="navbar-toggle" href="#nav-menu">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span> </a>
                                <!-- LOGO & LOGO STICKY  -->
                                <a class="navbar-brand" href="./"><img src="images/logo1.png" alt="logo" width="155" height="25"><img src="images/logo1.png" class="sticky-logo" alt="logo" width="155" height="25"> </a>
                            </div>
                            <!-- /. TOGGLE ICON -->
                            
                            <!-- MAIN MENU -->
                            <div id="nav-menu">
                                <ul class="nav navbar-nav navbar-right">
                                    <!-- DROPDOWN -->
									<li class="dropdown mega-dropdown scroll phone">
                                        <a href="tel:88001004365">8 800 100-43-65</a>                                        
                                    </li>
                                    <li class="dropdown mega-dropdown scroll">
                                        <a href="index.php">Главная</a>                                        
                                    </li>
                                    <!-- MENU -->
                                    <!-- MEGA MENU -->
                                    <li class="dropdown mega-dropdown scroll">
                                        <a href="our-business.php">О компании</a>                                       
                                    </li>

                                    <!-- MEGA MENU -->
                                    <li class="dropdown">
                                        <a href="our_services.php">Услуги<span class="caret"></span></a>
                                        <!-- DROPDOWN MENU -->
                                        <ul class="dropdown-menu">
                                            <li><a href="ourstrategy.php">Регистрация иностранных компаний</a></li>
                                            <li><a href="open_account.php">Открытие банковских счетов </a></li>  
											<li><a href="residence.php">Получение вида на жительства</a></li>                             
                                        </ul>
                                    </li>                             
                                    <!-- MEGA MENU -->
                                    <li class="dropdown mega-dropdown scroll"><a href="contact-us.php">Контакты</a>
                                    </li>
                                </ul>

                            </div>
                            <!-- /. MAIN MENU -->
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                </div>
                <!-- /. STICKY WRAPPER -->
            </header>
            <!-- /. HEADER -->