<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Гонконг</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Гонконг</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a"><img class="img-responsive" width="960" height="640" src="images/flags/honkong.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Гонконг
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="low_tax_cyprus.php"  class="list-group-item">Кипр</a>
							<a href="low_tax_switzerland.php" class="list-group-item">Швейцария</a>                                       
							<a href="low_tax_england.php" class="list-group-item">Англия (LLC)</a>
							<a href="low_tax_scotland.php" class="list-group-item">Шотландия (LLC)</a>
							<a href="low_tax_jerci.php" class="list-group-item">Остров Джерси</a>
							<a href="low_tax_honkong.php" class="list-group-item active">Гонконг</a>
							<a href="low_tax_singapore.php" class="list-group-item">Сингапур</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании в Гонконге	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Базовый пакет</th>
						<th>Стандартный пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					<tr>
						  <td>Проверка названия</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Регистрация компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оплата 	госпошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление уставных документов</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Предоставление юридического адреса на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга агента на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Доставка документов курьерской службой</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление доп. комплекта документов под апостилем</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального директора на один год</td>
						  <td></td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального акционера на один год</td>
						  <td></td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					 
					  <tr>
						  <td>Общая стоимость</td>
						  <td>1000$</td>
						  <td>$1250</td>
						  <td>$1600</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>750$</td>
						  <td>1000$</td>
						  <td>1400$</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Основные особенности компании в Гонконге.	
				</span>								
				</h3>
				<p>
				
				<table class="table no-border text-center">
					
					<tbody>
						<tr>
						  <td>Организационно-правовая форма</td>
						  <td>Limited</td>
					  </tr>
					  <tr>
						  <td>Система права в стране</td>
						  <td>Английское право</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность реестра</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Раскрытие данных о бенефициаре</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования к акционерам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество акционеров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности акционеров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускаются акционеры-юридические лица</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к директорам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество директоров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности директоров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли директор-юридическое лицо</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к секретарю</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требование по наличию секретаря</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности секретаря</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Допускается ли секретарь-юридическое лицо</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Уставный капитал и акции</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальный размер объявленного капитала</td>
						  <td>Нет ограничений, но обычно 10,000 HKD</td>
					  </tr>
					  <tr>
						  <td>Минимальный размер оплаченного капитала</td>
						  <td>1 HKD</td>
					  </tr>
					  <tr>
						  <td>Срок оплаты объявленного капитала</td>
						  <td>Нет ограничений</td>
					  </tr>
					  <tr>
						  <td>Выпуск акций на предъявителя</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Налогообложение в Гонконге</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Налог на прибыль</td>
						  <td>16.5%</td>
					  </tr>
					  <tr>
						  <td>НДС</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Налог у источника (дивиденды, проценты)</td>
						  <td>0%</td>
					  </tr>
					  <tr>
						  <td>Налог на прирост капитала</td>
						  <td>0%</td>
					  </tr>
					  <tr>
						  <td>Дополнительные обязательные местные налоги</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Валютный контроль</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования по отчетности</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требования по сдаче отчетности</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требование по подаче Annual Return</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Открытый доступ к отчетности</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Дополнительная информация</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Членство в ОЭСР (OECD)</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Количество подписанных Соглашений об избежании двойного налогообложения</td>
						  <td>29</td>
					  </tr>
					  <tr>
						  <td>Наличие Соглашения об избежании двойного налогообложения с Россией</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Группа риска: ЦБ РФ N 1317-У</td>
						  <td>Да (группа 1)</td>
					  </tr>
					  <tr>
						  <td>Группа риска: Минфин РФ Приказ N 108н</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации новой компании</td>
						  <td>1 день</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Особенности регистрации компании в Гонконге

				</span>								
				</h3>
				<ul>
					<li>Общество с ограниченной ответственностью или акционерное общество обязано иметь в составе управляющего в лице 1 директора и 1 акционера. Эти должности занимают только нерезиденты Китая любой национальности, возможно совмещение двух должностей.</li>
					<li>Для регистрации компании в Гонконге необходимо предоставление пакета всех материалов. Переводить все на китайский язык не обязательно, практически всегда сведения отражаться на латинице.</li>
					<li>Для создания фирмы в Гонконге у вас должен быть физический адрес, которым мы можем вас снабдить.</li>
					<li>Обязательным критерием является приобретение одной или любого количества акций номиналом НК $1, акции приобретаются как часть взноса уставного капитала фирмы.</li>
					<li>При открытии компании в Гонконге назначается генеральный директор, гражданин или нерезидент административного региона, но секретарем может быть нанят только гражданин КНР.</li>
					<li>При учреждении утверждается имя фирмы. Оно составляется на китайском или английском языках, возможно смешанное наименование. Наши специалисты рекомендуют предложить на выбор сразу несколько названий.</li>
					
				</ul>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>