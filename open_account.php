<?php
include 'header_inner.php';
?>
		
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights" data-animated="fadeInLeftBig" data-animation-delay="400">Открытие банковских счетов</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="index.html">Главная</a>
					</li>
					<li>
						<a href="ourstrategy.html">Услуги</a>
					</li>
					<li class="typo-dark">Открытие банковских счетов </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- PAGE HEADING SECTION -->
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
	<div class="row">
		<div class="col-md-9 top-margin-md">
		<p class="text-justify">
		Размещение денежных средств на счете в зарубежном банке - надежный способ сохранения и приумножения своих сбережений. Принятие решения о размещении средств за рубежом для физических лиц, в большей степени обосновано потребностью защиты своих сбережений. Для юридических лиц наличие счета в оптимально подобранной банковской юрисдикции упрощает ведение бизнеса и способствует полноценной работе компании с учетом всех ее особенностей, придает статус и повышает уровень репутации, как делового партнера.
		</p>
		<p class="text-justify">
		Специалисты Атласрег предложат Вам профессиональные консультации по вопросам выбора банка и юрисдикции, соответствующие Вашим личным целям и требованиям или особенностям Вашего бизнеса. Наши специалисты обеспечат Вам полное сопровождение процесса открытия счета и подготовку всех необходимых документов, а также представление Ваших интересов в переговорах с банком. Команда Атласрег поможет вам с минимальными временными затратами реализовать цели и задачи, поставленные при открытии счета за рубежом.
		</p>
		</div>
		<div class="col-md-3 sidebar">
				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="account_cyprus.php"  class="list-group-item">Счета на Кипре</a>
							<a href="#" class="list-group-item">Счета в Лихтенштейне</a>                                       
							<a href="account_switzerland.php" class="list-group-item">Счета в Швейцарии</a>
							<a href="#" class="list-group-item">Счета в Латвии</a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
	</div>
		<div class="row">
			<div class="col-md-9 top-margin-md">
				<!-- POST -->
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							КИПР
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<table class="table">
						<thead>
						  <tr>
							<th>Банк</th>
							<th>Тип счета</th>
							<th>Стоимость (EUR)</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>Bank of Сyprus</td>
							<td>Расчетный (личный и корпоративный)</td>
							<td>185 - 500</td>
						  </tr>
						  <tr>
							<td>Eurobank Cyprus Ltd</td>
							<td>Расчетный (личный и корпоративный)</td>
							<td>185 - 500</td>
						  </tr>
						  <tr>
							<td>Piraeus Bank</td>
							<td>Расчетный (личный и корпоративный)</td>
							<td>185 - 500</td>
						  </tr>
						  <tr>
							<td>RCB Bank</td>
							<td>Расчетный (личный и корпоративный)</td>
							<td>185 - 500</td>
						  </tr>
						  <tr>
							<td>Hellenic Bank</td>
							<td>Расчетный (личный и корпоративный)</td>
							<td>185 - 500</td>
						  </tr>
						</tbody>
					</table>
					</div>
				</div>
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							ШВЕЙЦАРИЯ
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<table class="table">
						<thead>
						  <tr>
							<th>Банк</th>
							<th>Тип счета</th>
							<th>Стоимость (EUR)</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>C.I.M. Banque</td>
							<td>Расчетный (личный и корпоративный)</td>
							<td>500</td>
						  </tr>
						  <tr>
							<td>UBS Bank</td>
							<td>Инвестиционный и расчетный(личный и корпоративный)</td>
							<td>500</td>
						  </tr>
						  
						</tbody>
					</table>
					</div>
				</div>
				<!-- POST -->
				
			</div>
			
		</div>
	</div>
</section>

<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>