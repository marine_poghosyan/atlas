<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="" data-animated="fadeInLeftBig" data-animation-delay="400">Счета на Кипре</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="open_account.php">Открытие банковских счетов </a>
					</li>

					<li class="typo-dark">Счета на Кипре</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="about-us" class="bottom-padding-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					
					<!-- TEXT -->
					<p class="text-justify">
					Если вас интересует ускоренное и практически беспроблемное открытие коммерческого счета, то выбор кипрского банка является идеальным вариантом.
					</p>
					<p>Преимущества банков Кипра:</p>
					<ul>
						<li>не предъявляют требования к неснижаемому остатку и объемам первого взноса;</li>
						<li>открытие счета в течении месяца;</li>
						<li>одна из лучших гарантий банковской тайны в мире;</li>
						<li>не усложненные требования к соответствию клиентов;</li>
						<li>быстрое и доступное открытие коммерческих счетов в короткие сроки.</li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 sidebar">
				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="bank_cyprus.php"  class="list-group-item">Bank of Cyprus</a>
							<a href="bank_piraeus.php" class="list-group-item">Piraeus Bank</a>                                       
							<a href="bank_hellenic.php" class="list-group-item">Hellenic Bank</a>
							<a href="#" class="list-group-item">RCB Bank</a>
							<a href="bank_eurobank.php" class="list-group-item">Eurobank Cyprus</a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
		</div>
			</div>
		</div>
		</div>
	</div>
</section>

<section id="about-us" class="bottom-padding-md no-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Процедура открытия счета
					</span>								
					</h3>
					<!-- TEXT -->
					<p>
					Среди особых условий открытия счета в кипрском банке необходимо отметить следующие:
					</p>
					<ul>
						<li>составление резюме не только на бенефициара, но и на распорядителя по счету (для корпоративных счетов);</li>
						<li>банковская справка о наличии счета от любого из банков, как для бенефициара, так и для распорядителя по счету (для корпоративных счетов);</li>
						<li>проведение идентификации директоров компании посредством предоставления апостилированного или оригинального паспорта.</li>
					</ul>
					<p>
					Обязательным условием любого кипрского банка является проведение личной встречи бенефициара по счету с представителем учреждения. Как правило, такую встречу, возможно организовать в нашем офисе или в представительстве банка.
					</p>
					<p>
					Учитывая членство Кипра в ЕС и присоединение страны к Еврозоне, в политике банков все больше прослеживается тенденция к соблюдению европейских требований, что приводит к постепенному усложнению процедуры открытия счета и его последующего использования. Сегодня банки требуют предоставления более детальной информации о движении средств по счету, а также наличия подтверждающих документов на проведение практически каждой транзакции.
					</p>
					<p class="bottom-padding-md">
					Наиболее популярные банки Кипра: <br>
						Bank of Cyprus <br>
						Piraeus Bank Ltd <br>
						Hellenic Bank Public Company LTD <br>
						RCB Bank <br>
						Eurobank Cyprus Ltd
					</p>
				</div>
			</div>
									 
		</div>
	</div>
		
</section>
	


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>