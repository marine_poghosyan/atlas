<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Эстония</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Эстония</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a"><img class="img-responsive" width="960" height="640" src="images/flags/estonia.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Эстония
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="european_hungary.php"  class="list-group-item">Венгрия</a>
							<a href="european_ireland.php" class="list-group-item">Ирландия</a>                                       
							<a href="#" class="list-group-item">Лихтенштейн</a>
							<a href="#" class="list-group-item">Мальта</a>
							<a href="#" class="list-group-item">Нидерланды</a>
							<a href="#" class="list-group-item active">Эстония</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании в Эстонии	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Наименование услуги</th>
						<th>Базовый пакет</th>
						<th>Стандартный пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					<tr>
						  <td> Регистрация компании, включая государственные пошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Нотариальные услуги</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Контактное лицо компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Юридический адрес на 1 год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Дистанционное открытие, включая перевод доверенности</td>
						  <td></td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Общая стоимость</td>
						  <td>€500</td>
						  <td>€1000</td>
						  <td>€1500</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>€100</td>
						  <td>€300</td>
						  <td>€300</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Основные особенности компании в Эстонии	
				</span>								
				</h3>
				<p>
				
				<table class="table no-border text-center">
					
					<tbody>
						<tr>
						  <td>Организационно-правовая форма</td>
						  <td>OU (Osauhing)</td>
					  </tr>
					  <tr>
						  <td>Система права в стране</td>
						  <td>Гражданское право</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность реестра</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Раскрытие данных о бенефициаре</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования к акционерам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество акционеров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности акционеров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускаются акционеры-юридические лица</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к директорам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество директоров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности директоров</td>
						  <td>Да (Как минимум 50% директоров должны быть резидентами ЕС)</td>
					  </tr>
					  <tr>
						  <td>Допускается ли директор-юридическое лицо</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования к секретарю</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требование по наличию секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли секретарь-юридическое лицо</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Уставный капитал и акции</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальный размер объявленного капитала</td>
						  <td>2,500 EUR</td>
					  </tr>
					  <tr>
						  <td>Минимальный размер оплаченного капитала</td>
						  <td>2,500 EUR</td>
					  </tr>
					  <tr>
						  <td>Срок оплаты объявленного капитала</td>
						  <td>100% перед регистрацией</td>
					  </tr>
					  <tr>
						  <td>Выпуск акций на предъявителя</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Налогообложение в Эстонии</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Налог на прибыль</td>
						  <td>0%</td>
					  </tr>
					  <tr>
						  <td>НДС</td>
						  <td>20%</td>
					  </tr>
					  <tr>
						  <td>Налог у источника (дивиденды, проценты)</td>
						  <td>Дивиденды - 21/79</td>
					  </tr>
					  <tr>
						  <td>Налог на прирост капитала</td>
						  <td>0%</td>
					  </tr>
					  <tr>
						  <td>Дополнительные обязательные местные налоги</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Валютный контроль</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования по отчетности</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требования по сдаче отчетности</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требование по подаче Annual Return</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Открытый доступ к отчетности</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Дополнительная информация</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Членство в ОЭСР (OECD)</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Количество подписанных Соглашений об избежании двойного налогообложения</td>
						  <td>57</td>
					  </tr>
					  <tr>
						  <td>Наличие Соглашения об избежании двойного налогообложения с Россией</td>
						  <td>Да, но еще не вступило в силу</td>
					  </tr>
					  <tr>
						  <td>Группа риска: ЦБ РФ N 1317-У</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Группа риска: Минфин РФ Приказ N 108н</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации новой компании</td>
						  <td>2-3 недели</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Преимущества регистрации компании в Эстонии	
				</span>								
				</h3>
				<p>
				Регистрация фирмы в Эстонии имеет ряд преимуществ. К примеру, компания не обязана уплачивать НДС при совершении ряда операций. Отсутствие валютного контроля со стороны властей. Несколько зон свободной торговли делают возможным приобретение иностранных товаров без уплаты пошлин. Регистрацию новой компании проводится в короткие сроки. 
Договоры об исключении двойного соглашения подписаны с более чем 50 странами. 
Нет налога на нераспределенную прибыль, он уплачивается после того, как компания распределит все дивиденды. Если же этого не происходит, компания ничего не отчисляет в бюджет.

				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>