<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="" data-animated="fadeInLeftBig" data-animation-delay="400">Классические оффшорные юрисдикции</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация зарубежных компаний</a>
					</li>

					<li class="typo-dark">Классические оффшорные юрисдикции </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="about-us" class="bottom-padding-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					
					<!-- TEXT -->
					<p class="text-justify">
					Регистрация компаний в классических оффшорных юрисдикциях обладает рядом преимуществ, среди которых можно выделить нулевую ставку при налогообложении при условии ведения деятельности вне пределов страны регистрации, отсутствие необходимости вести и подавать бухгалтерскую и налоговую отчетности.
					</p>
					<p class="text-justify">
					Единственным условием ведения деятельности оффшорной компании является  уплата фиксированной пошлины. Размер такой пошлины, как правило, составляет 100-350 USD в год.
					</p>
					<p class="text-justify">
					Еще одним немаловажным фактором для тех, кто хочет зарегистрировать компанию в классических оффшорных юрисдикциях, является соблюдение максимальной конфиденциальности. Государственные органы, осуществляющие регистрацию юридических лиц, не ведут реестры директоров и акционеров компаний и, соответственно, возможность получения третьими лицами информации о структуре компании минимальна.
					</p>
																
				</div>
			</div>
			<div class="col-md-3 sidebar">
				<div class="widget">
					<h5 class="widget-title">Страны</h5>
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="offshore_seishel.php"  class="list-group-item">Сейшельские Острова</a>
							<a href="offshore_bahamas.php" class="list-group-item">Багамские Острова</a>                                       
							<a href="offshore_virgins.php" class="list-group-item">Британские Виргинские Острова</a>
							<a href="offshore_kaiman.php" class="list-group-item">Каймановы Острова</a>
							<a href="offshore_beliz.php" class="list-group-item">Белиз</a>
							<a href="offshore_panama.php" class="list-group-item">Панама</a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
		</div>
			</div>
		</div>
		</div>
	</div>
</section>

<section id="about-us" class="bottom-padding-md no-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Основные недостатки классических оффшорных юрисдикций
					</span>								
					</h3>
					<!-- TEXT -->
					<ul>
						<li>Отсутствие или неприменимость Соглашений об избежании двойного налогообложения;</li>
						<li>Присутствие большинства классических оффшорных юрисдикций в так называемых «черных» списках Российской Федерации и международных сообществ (ОЭСР, ФАТФ). </li>
					</ul>
				</div>
			</div>
									 
		</div>
	</div>
		
</section>

<section id="about-us" class="bottom-padding-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Стоимость регистрации компании в классических оффшорных юрисдикциях
					</span>								
					</h3>
					<!-- TEXT -->
					<p>
					Стоимость регистрации компании в классических оффшорных юрисдикциях

					</p>
					<p class="table">
					<table class="table">
						<thead>
						  <tr>
							<th>Страна</th>
							<th>Регистрация компании Базовый пакет</th>
							<th>Регистрация компании Полный пакет</th>
							<th>Стоимость обслуживания со второго года</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td class="text-center" colspan=4>АФРИКА</td>
						  </tr>
						  <tr>
							<td>Сейшельские острова</td>
							<td>$450</td>
							<td>$700</td>
							<td>$400</td>
						  </tr>
						  <tr>
							<td class="text-center" colspan=4>КАРИБСКИЙ РЕГИОН</td>
							
						  </tr>
						  <tr>
							<td>Багамские Острова</td>
							<td>$1500</td>
							<td>$1800</td>
							<td>$1400</td>
						  </tr>
						  <tr>
							<td>Британские Виргинские Острова</td>
							<td>$1000</td>
							<td>$1250</td>
							<td>$750</td>
						  </tr>
						  <!--<tr>
							<td>Каймановы Острова</td>
							<td>от $3600</td>
							<td>$5300</td>
							<td>от $3600</td>
						  </tr>-->
						   <tr>
							<td class="text-center" colspan=4>ЦЕНТРАЛЬНАЯ АМЕРИКА</td>
							
						  </tr>
						  <tr>
							<td>Белиз</td>
							<td>$800</td>
							<td>$1200</td>
							<td>$800</td>
						  </tr>
						  <tr>
							<td>Панама</td>
							<td>$1000</td>
							<td>$1550</td>
							<td>$1050</td>
						  </tr>
						</tbody>
					</table>
					</p>										
				</div>
			</div>
									 
		</div>
	</div>
		
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>