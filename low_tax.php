<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="" data-animated="fadeInLeftBig" data-animation-delay="400">Низконалоговые юрисдикции</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация зарубежных компаний</a>
					</li>

					<li class="typo-dark">Низконалоговые юрисдикции</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="about-us" class="bottom-padding-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					
					<!-- TEXT -->
					<p  class="text-justify">
					Эти страны не имеют специального оффшорного законодательства, и в некоторых из них установлены очень 
					низкие ставки налогов. Положения налогового законодательства этих государств могут также предусматривать 
					освобождение от налогообложения доходов от отдельных видов деятельности (в частности, полученных вне места 
					регистрации юридического лица), либо возможность исчислять налоги на такие виды доходов по пониженным 
					ставкам. В некоторых случаях, с учетом этих правил, налоговая база и, как следствие суммы подлежащих 
					уплате налогов, будут равны нулю.
					<br>
					Низконалоговые юрисдикции предусматривают, как правило, обязанность юридических лиц вести и предоставлять 
					бухгалтерскую и налоговую отчетность, исчислять и уплачивать налоги, необходимость иметь в качестве 
					директоров компании резидентов соответствующей юрисдикции. Как следствие, поддержание таких компаний 
					более трудоемкое и дорогое, в сравнении с классическими оффшорными юрисдикциями.
					<br>
					Тем не менее, возможность использовать льготы по Соглашениям об избежании двойного налогообложения зачастую 
					обеспечивает значительную налоговую экономию, которая окупает затраты на создание и поддержание компании 
					из налоговой юрисдикции.
					</p>							
				</div>
			</div>
			<div class="col-md-3 sidebar">
				<div class="widget">
					<h5 class="widget-title">Страны</h5>
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="low_tax_cyprus.php"  class="list-group-item">Кипр</a>
							<a href="low_tax_switzerland.php" class="list-group-item">Швейцария</a>                                       
							<a href="low_tax_england.php" class="list-group-item">Англия (LLC)</a>
							<a href="low_tax_scotland.php" class="list-group-item">Шотландия (LLC)</a>
							<a href="low_tax_jerci.php" class="list-group-item">Остров Джерси</a>
							<a href="low_tax_honkong.php" class="list-group-item">Гонконг</a>
							<a href="low_tax_singapore.php" class="list-group-item">Сингапур</a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
		</div>
			</div>
		</div>
		</div>
	</div>
</section>

<section id="about-us" class="bottom-padding-md no-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Стоимость оформления
					</span>								
					</h3>
					<!-- TEXT -->
					<p>
					Компания Atlasreg работает со следующими юрисдикциями:

					</p>
					<p class="table">
					<table class="table">
						<thead>
						  <tr>
							<th>Страна</th>
							<th>Регистрация компании</th>
							<th> Стоимость комплекта «все включено»</th>
							<th> Ежегодный сбор для компаний</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>Кипр</td>
							<td>от $1700</td>
							<td>$2300</td>
							<td>от $1500</td>
						  </tr>
						  <tr>
							<td>Швейцария</td>
							<td>от €9900</td>
							<td>от €9900</td>
							<td>от €8900</td>
						  </tr>
						  <tr>
							<td>Англия (LLC)</td>
							<td>от $1450</td>
							<td>$1650</td>
							<td>от $1200</td>
						  </tr>
						  <tr>
							<td>Шотландия (LLC)</td>
							<td>от $1450</td>
							<td>$1750</td>
							<td>от $1150</td>
						  </tr>
						  <tr>
							<td>Остров Джерси</td>
							<td>от $4000</td>
							<td>$5300</td>
							<td>от $3500</td>
						  </tr>
						  <tr>
							<td>Гонконг</td>
							<td>от $1900</td>
							<td>$2900</td>
							<td>от $1500</td>
						  </tr>
						  <tr>
							<td>Сингапур</td>
							<td>от $5000</td>
							<td>$6800</td>
							<td>от $4500</td>
						  </tr>
						</tbody>
					</table>
					</p>										
				</div>
			</div>
									 
		</div>
	</div>
		
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>