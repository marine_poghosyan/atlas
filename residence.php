<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Получение вида на жительства</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li class="typo-dark">Получение вида на жительства</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar ">
	<div class="container">
		<div class="row bottom-margin-md" data-animation="fadeInUp" data-animation-delay="400">
			
		</div>
		<div class="row">
			<div class="col-md-9">
				<!-- COLUMN 1/2 -->
				<div class="row bottom-margin-md" data-animation="fadeInUp" data-animation-delay="400">
					
					<div class="col-sm-12">

						
						<p class="text-justify">
						Вид на жительство — первая ступень к получению гражданства. Причем в каждой стране действуют свои условия выдачи этого документа (покупка недвижимости, обучение, трудоустройство, семейные отношения). Он подтверждает право иностранного гражданина либо лица без гражданства проживать в государстве пребывания. По сроку действия различают временные (в большинстве стран ЕС — 5 лет) и постоянные (ПМЖ) разрешения.

Какие привилегии дает вид на жительство за рубежом?
Чаще всего россияне получают ВНЖ, чтобы жить и работать в интересующей их стране, свободно перемещаться по Шенгенской зоне, иметь социальные гарантии и более высокий уровень жизни.

						</p>
					</div>
				</div>
				<!-- /.COLUMN 1/2 -->
				<!-- PIE CHARTS STYLES -->
			</div>
			<div class="col-md-3 sidebar no-padding">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="#"  class="list-group-item">ОАЭ</a>
							<a href="#" class="list-group-item">Панама</a>                                       
							<a href="#" class="list-group-item">Европейские компании</a> 
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
</section>
<section id="about-us" class="bottom-padding-md padding-top-10">
	<div class="container">
		
		<div class="row bottom-padding-md">
			<div class="col-md-9 col-sm-12">
				<div class="section-title no-margin">
					<!-- TITLE -->
					<h3 class="parallax-title-1">
					<span class="text-style"> 
					Оформление ВНЖ дает следующие привилегии:
					</span> </br>
					</h3>
					<!-- TEXT -->
					
					<ul>
						<li>возможность находиться на территории страны в любое время (в пределах срока действия документа);</li>
						<li>свободный въезд-выезд;</li>
						<li>социальные права и гарантии;</li>
						<li>доступ к качественным медицинским услугам и образованию (бесплатно или по льготной цене);</li>
						<li>оформление кредита и ипотеки по низким ставкам;</li>
						<li>возможность в дальнейшем получить гражданство (в ЕС — через 10 лет).</li>
						
					</ul>
					<p>
					Мы оказываем комплексные услуги по получению ВНЖ в разных странах, включая оформление и регистрацию документов. Решать миграционные вопросы с нами быстро и легко!
					</p>
					<p>
					Для получения вида на жительства от Вас потребуется:
					</p>
					<ul>
						<li>Загранпаспорт;</li>
						<li>Ксерокопия общегражданского паспорта;</li>
						<li>Справка о несудимости, фотографии.</li>
					</ul>
					<p>
					Список документов, необходимых для получения вида на жительство, зависит от страны, выбранной Вами.
					</p>
				</div>
			</div>
		</div>
		</div>
	</div>
</section>

<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>