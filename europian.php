<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="" data-animated="fadeInLeftBig" data-animation-delay="400">Европейские юрисдикции</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация зарубежных компаний</a>
					</li>

					<li class="typo-dark">Европейские юрисдикции</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="about-us" class="bottom-padding-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					
					<!-- TEXT -->
					<p  class="text-justify">
					К таким юрисдикциям относятся, как правило, юрисдикции Европы и Северной Америки. Регистрация и содержание компании в таких странах достаточно дороги, а уровень налоговой нагрузки традиционно высокий, однако, приобретение такой компании позволит достойно представлять себя на конкурентном рынке, пользоваться международными соглашениями в области налогообложения и защиты прав и не связано ни с какими ограничениями по «черным спискам». Следует заметить, что зачастую локальное законодательство этих стран содержит налоговые льготы и корпоративные инструменты, использование которых позволяет заметно снизить общую налоговую нагрузку (иногда – до нулевого уровня).
					</p>
					<p >
					Основными преимуществами европейских компаний являются:
					</p>
					<ul>
						<li>Высокий уровень доверия со стороны поставщиков и клиентов;</li>
						<li>Безопасность активов и минимизация юридических и фактических рисков;</li>
						<li>Право осуществлять деятельность на территории всего Европейского союза;</li>
						<li>Высокий уровень развития банковской системы;</li>
						<li>Низкие процентные ставки по кредитам;</li>
						<li>Множество соглашений об избежании двойного налогообложения;</li>
						<li>Устойчивая экономическая и политическая ситуация;</li>
						<li>Возможность получения вида на жительство и гражданства Евросоюза.</li>
					</ul>
																
				</div>
			</div>
			<div class="col-md-3 sidebar">
				<div class="widget">
					<h5 class="widget-title">Страны</h5>
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="european_hungary.php"  class="list-group-item">Венгрия</a>
							<a href="european_ireland.php" class="list-group-item">Ирландия</a>                                       
							<a href="#" class="list-group-item">Лихтенштейн</a>
							<a href="#" class="list-group-item">Мальта</a>
							<a href="#" class="list-group-item">Нидерланды</a>
							<a href="european_estonia.php" class="list-group-item">Эстония</a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
		</div>
			</div>
		</div>
		</div>
	</div>
</section>

<section id="about-us" class="bottom-padding-md no-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Стоимость оформления
					</span>								
					</h3>
					<!-- TEXT -->
					<p>
					Компания Atlasreg работает со следующими юрисдикциями:

					</p>
					<p class="table">
					<table class="table">
						<thead>
						  <tr>
							<th>Страна</th>
							<th>Регистрация компании</th>
							<th> Стоимость комплекта «все включено»</th>
							<th> Ежегодный сбор для компаний</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>Венгрия</td>
							<td>от €1800</td>
							<td>от €2800</td>
							<td>от €1600</td>
						  </tr>
						  <tr>
							<td>Ирландия</td>
							<td>от €1900</td>
							<td>от €2100</td>
							<td>от €1400</td>
						  </tr>
						  <tr>
							<td>Мальта</td>
							<td>от €3500</td>
							<td>от €5500</td>
							<td>от €2600</td>
						  </tr>
						  <tr>
							<td>Нидерланды</td>
							<td>от €2900</td>
							<td>от €10000</td>
							<td>от €2500</td>
						  </tr>
						  <tr>
							<td>Эстония</td>
							<td>от €500</td>
							<td>от €1200</td>
							<td>от €100</td>
						  </tr>
						</tbody>
					</table>
					</p>										
				</div>
			</div>
									 
		</div>
	</div>
		
</section>

<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>