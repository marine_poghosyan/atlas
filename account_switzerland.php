<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="" data-animated="fadeInLeftBig" data-animation-delay="400">Счета в Швейцарии</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="open_account.php">Открытие банковских счетов </a>
					</li>

					<li class="typo-dark">Счета в Швейцарии</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="about-us" class="bottom-padding-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					
					<!-- TEXT -->
					<p class="text-justify">
					Швейцария – банковская юрисдикция, которая является символом банковской тайны. Благодаря чтению традиций конфиденциальности, Швейцария много лет занимает лидирующие позиции в сфере открытия банковских счетов для сохранения активов.
					</p>
					
				</div>
			</div>
			<div class="col-md-3 sidebar">
				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="#"  class="list-group-item"></a>
							<a href="#" class="list-group-item"></a>                                       
							<a href="#" class="list-group-item"></a>
							<a href="#" class="list-group-item"></a>
							<a href="#" class="list-group-item"></a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
		</div>
			</div>
		</div>
		</div>
	</div>
</section>

<section id="about-us" class="bottom-padding-md no-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Цели использования счетов в банках Швейцарии
					</span>								
					</h3>
					<!-- TEXT -->
					<p>
					Основным направлением швейцарских банков остается открытие сберегательных и инвестиционных счетов в сочетании с услугами private banking, которые направлены на приумножение средств клиента. Типы счетов в этом случае могут быть как личными, так и корпоративными. Расчётные операции банки страны проводят в исключительных случаях, и при условии наличия большой суммы на счете.
					</p>
					
					<p>
					Открытие расчетных счетов – неординарная услуга швейцарских банков, однако некоторые из них (например, CIM Banque) готовы ее предложить. При этом использование швейцарского счета такого типа приводит к существенному удорожанию входящих и исходящих платежей в зависимости от суммы операции.
					</p>
					<p>
					Наиболее лояльные банки в Швейцарии с высокими рейтингами: <br>
CIM Banque <br>
Julius Baer&Co.
					</p>
					
				</div>
			</div>
									 
		</div>
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Специфика открытия счета в банке Швейцарии
					</span>								
					</h3>
					<!-- TEXT -->
					<ul>
						<li>Предоставление детальной информации о происхождении активов компании или средств физического лица.</li>
						<li>Предоставление детальной информации о состоятельности клиента (бенефициарного владельца компании или физического лица).</li>
						<li>Длительная процедура рассмотрения документов отделом комплаенс.</li>
					</ul>
				</div>
			</div>
									 
		</div>
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Помощь специалистов
					</span>								
					</h3>
					<!-- TEXT -->
					<p>
					Юристы Атласрег помогут подобрать банк в Швейцарии, который наиболее точно соответствует вашим требованиям и особенностям деятельности компании. Благодаря налаженному сотрудничеству со многими швейцарскими банками, мы можем предложить более упрощенную и быструю процедуру открытия счета.
					<br>
					Швейцарские банки требуют проведения личной встречи представителя учреждения с владельцем компании или физическим лицом, открывающим личный счет. Партнерские отношение с банками этого государства позволяют нам организовать выезд банкиров для организации встречи в нашем офисе, а также провести идентификацию наших клиентов в телефонном режиме или посредством Skype-конференции.
					<br>
					Использование наших услуг по сопровождению открытия счета в Швейцарии даст возможность клиенту получить квалифицированные рекомендации по выбору надежного и подходящего банка, а также сэкономить время, усилия и средства, которые могут быть потрачены при самостоятельном открытии счета.
					</p>
				</div>
			</div>
									 
		</div>
	</div>
		
</section>
	


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>