<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Багамские острова</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Багамские острова</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a><img class="img-responsive" width="960" height="640" src="images/flags/Bahamas.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Багамские острова
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="offshore_seishel.php"  class="list-group-item">Сейшельские Острова</a>
							<a href="offshore_bahamas.php" class="list-group-item active">Багамские Острова</a>                                       
							<a href="offshore_virgins.php" class="list-group-item">Британские Виргинские Острова</a>
							<a href="offshore_kaiman.php" class="list-group-item">Каймановы Острова</a>
							<a href="offshore_beliz.php" class="list-group-item">Белиз</a>
							<a href="offshore_panama.php" class="list-group-item">Панама</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании на Багамских островах	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Базовый пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					  <tr>
						  <td>Проверка названия</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Регистрация компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оплата госпошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление уставных документов</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Предоставление юридического адреса на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга агента на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Доставка документов курьерской службой</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление доп. комплекта документов под апостилем</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального директора на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального акционера на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Общая стоимость</td>
						  <td></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td></td>
						  <td></td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
				
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Основные особенности оффшорной компании на Багамских островах
				</span>								
				</h3>
				<p>
				
				<table class="table one-third">
					<thead>
					  <tr>
						<th>Параметры</th>
						<th>Значения</th>										
					  </tr>
					</thead>
					<tbody>
					  <tr>
						  <td>Тип компании</td>
						  <td>IBC (International Business Company)</td>
					  </tr>
					  <tr>
						  <td>Название</td>
						  <td>Должно оканчиваться словами "Ltd", "Limited", "Corporation", "Corp.", "Incorporated", "Inc" и др.</td>
					  </tr>
					  <tr>
						  <td>Корпоративное законодательство</td>
						  <td>The International Business Companies Act, 2000</td>
					  </tr>
					  <tr>
						  <td>Уставной Капитал</td>
						  <td>50.000 USD (не требует оплаты)</td>
					  </tr>
					  <tr>
						  <td>Акции на предъявителя</td>
						  <td>Не разрешены</td>
					  </tr>
					  <tr>
						  <td>Мин. количество акционеров</td>
						  <td>1 (может быть юр. лицом)</td>
					  </tr>
					  <tr>
						  <td>Номинальные акционеры</td>
						  <td>Разрешены</td>
					  </tr>
					  <tr>
						  <td>Мин. количество директоров</td>
						  <td>1 (может быть юр. лицом)</td>
					  </tr>
					  <tr>
						  <td>Номинальные директора</td>
						  <td>Разрешены</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность</td>
						  <td>Высокая. В стране закрытый регистр акционеров и директоров. Информация предоставляется только по решению суда. При использовании номинальных директоров и акционеров истинные владельцы компании вообще не могут быть установлены.</td>
					  </tr>
					  <tr>
						  <td>Проведение ежегодных собраний</td>
						  <td>Не требуется. При необходимости могут быть проведены в любой стране мира (возможно по телефону).</td>
					  </tr>
					  <tr>
						  <td>Сдача ежегодной отчетности</td>
						  <td>Не требуется</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Не требуется</td>
					  </tr>
					  <tr>
						  <td>Налогообложение</td>
						  <td>Отсутствует</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации и доставки документов</td>
						  <td>2 недели</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Особенности регистрации компании на Багамских островах
				</span>								
				</h3>
				<p class="bottom-padding-md">
				Для регистрации оффшорной компании на территории Багамских островов необходимо предоставить устав и учредительный договор создаваемого предприятия. Стоит учесть, что директором и акционерами оффшора на Багамских островах могут быть как юридические, так и физические лица. Допускается совмещение роли директора и акционера.
<br>Открывая широкие перспективы для развития бизнеса, оффшор Багамские острова, тем не менее, отличается определенными ограничениями. В частности, оффшорной компании не разрешается предоставление адреса зарегистрированного офиса для нужд других организаций. Предприятию, зарегистрированному на Багамах, разрешено вкладывать средства в содержание офиса, однако запрещаются инвестиции в иную недвижимость на Багамских островах. Оффшорная компания также не имеет права вести какой-либо бизнес с резидентами Багамских островов.

				</p>
				
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Порядок и процедура регистрации компании на Багамских островах
				</span>								
				</h3>
				<ul class="bottom-padding-md">
					<li>Проверка и подтверждение имени компании в регистре;</li>
					<li>Подготовка и заполнение всех необходимых документов;</li>
					<li>Оплата необходимых регистрационных пошлин и сборов;</li>
					<li>Инкорпорация компании в Реестре Компаний;</li>
					<li>Нотариальное заверение и апостилирование (при необходимости);</li>
					<li>Доставка документов клиенту курьерской почтой.</li>
				</ul>
			</div>
		</div>
	</div>
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>