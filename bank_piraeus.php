<?php
include 'header_inner.php';
?>
		
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights" data-animated="fadeInLeftBig" data-animation-delay="400">​PIRAEUS BANK CYPRUS (КИПР)</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="index.html">Главная</a>
					</li>
					<li>
						<a href="open_account.php">Открытие банковских счетов </a>
					</li>
					<li class="typo-dark">​PIRAEUS BANK CYPRUS (КИПР)</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- PAGE HEADING SECTION -->
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
	<div class="row">
		<div class="col-md-9 top-margin-md">
		<p class="text-justify">
		Сайт банка –  <a href="www.piraeusbank.com.cy">www.piraeusbank.com.cy</a> <br>
Расположение банка – Кипр, Никосия. <br>
Дочерняя структура греческого "Piraeus Bank S.A." Банковское учреждение зарегистрировано на Кипре, а его деятельность регулируется ЦБ Кипра.
		</p>
		</div>
		<div class="col-md-3 sidebar">
				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="bank_cyprus.php"  class="list-group-item">Bank of Cyprus</a>
							<a href="bank_piraeus.php" class="list-group-item active">Piraeus Bank</a>                                       
							<a href="bank_hellenic.php" class="list-group-item">Hellenic Bank</a>
							<a href="#" class="list-group-item">RCB Bank</a>
							<a href="bank_eurobank.php" class="list-group-item">Eurobank Cyprus</a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
	</div>
		<div class="row">
			<div class="col-md-9 ">
				<!-- POST -->
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Основные характеристики банка
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<table class="table">
						<thead>
						 
						</thead>
						<tbody>
						  <tr>
							<td>СРОК ОТКРЫТИЯ СЧЕТА</td>
							<td>
							Реквизиты счетов предоставляются в течение двух-трех недель.
Диджипасс выдается во время встречи с представителем банка.
Коды для доступа к счету предоставляются по e-mail в течение 1-2 рабочих дней после первого поступления денежных средств на счет.

							</td>
						  </tr>
						  <tr>
							<td>ТИПЫ СЧЕТОВ</td>
							<td>Личные Корпоративные</td>
						  </tr>
						  <tr>
							<td>НАЗНАЧЕНИЯ СЧЕТОВ</td>
							<td>Сберегательные Расчетные</td>
						  </tr>
						  <tr>
							<td>ПЕРВОНАЧАЛЬНЫЙ ВЗНОС</td>
							<td>Активационный платеж: <br>
							300 EUR/USD - для юридических лиц <br>
							200 EUR/USD - для физических лиц
							</td>
						  <tr>
							<td>НЕСНИЖАЕМЫЙ ОСТАТОК</td>
							<td>Нет требований</td>
						  </tr>
						  <tr>
							<td>ПЕРСОНАЛ</td>
							<td>Русскоговорящий Англоговорящий</td>
						  </tr>
						  <tr>
							<td>УПРАВЛЕНИЕ СЧЕТОМ</td>
							<td>Через Интернет-банкинг (диджипасс) и по факсу.</td>
						  </tr>
						  <tr>
							<td>ТАРИФЫ</td>
							<td>
							Обслуживание счета в год (включая Интернет-банкинг и управление по факсу):
200 EUR для корпоративных и личных счетов, плюс ежегодное обновление клиентского файла (деятельность, обороты, контрагенты и др.) – 100 EUR min.
Закрытие счета – 200 EUR.
Хранение корреспонденции в банке в год: 200 EUR для физических лиц и 300 EUR для юридических лиц.
Одноразовая плата за устройство Диджипасс - 40 EUR.
Конвертация валют: 0,5% (мин. 5 EUR / макс. 400 EUR).

Исходящие платежи (в зависимости от суммы и страны отправителя платежа):
В евро: комиссия за транзакцию - 0.15% min 5 EUR / max 400 EUR, плюс обработка - 12 EUR.
В другой валюте: комиссия за транзакцию - 0.15% min 5 EUR / max 400 EUR, плюс обработка - 12 EUR.
Входящие платежи:
В евро:
до 10 000 EUR - без комиссии;
10 000 EUR - 50 000 EUR: комиссия 4 EUR;
Более 50 000 EUR - 30 EUR.
В другой валюте:
до 2 000 EUR - комиссия 2 EUR;
2 000 EUR - 50 000 EUR: комиссия 10 EUR;
более 50 000 EUR - комиссия 40 EUR.

							</td>
						  </tr>
						  <tr>
							<td>ПЛАСТИКОВЫЕ КАРТЫ</td>
							<td>
							Дебетовые карты (выпуск в EUR, USD, GBP, CHF).
Годовое обслуживание карт в EUR:
для личных счетов: Visa Classic, Visa Electron – бесплатно, Visa Gold – 42,5 EUR, Visa Platinum – 85 EUR.
Для корпоративных счетов: Visa Business Card – 25 EUR.
Годовое обслуживание карт в USD:
Для личных счетов: Visa Electron – бесплатно, Visa Classic – 20 USD, Visa Gold – 50 USD, Visa Platinum – 100 USD.
Для корпоративных счетов: Visa Business Card - 30 USD.
Гарантийный депозит – нет требований, кроме карты Visa Platinum - 10 000 USD или эквивалент.
Снятие наличных:
Евро – 2,7 EUR за транзакцию.
Доллары – 3,33% (мин. 5,7 USD) + 3,5 USD.
Оплата товаров и услуг – без комиссии.
Лимит на снятие наличных в день - 2000 Евро/3000 USD установлен по всем картам и может быть увеличен на определенный период (например, под какую-либо покупку) по согласованию с банком. Возможно оформление кредитных банковских карт платежной системы American Express (тарифы устанавливаются индивидуально после рассмотрения заявки).

							</td>
						  </tr>
						  
						</tbody>
					</table>
					</div>
				</div>
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Особенности и сервисы банка
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<p>
						Обслуживание зарубежных клиентов ведется в четырех специализированных международных бизнес-центрах, расположенных в Лимасоле, Никосии и Ларнаке. Банковское учреждение предоставляет своим клиентам:
						</p>
						<ul>
							<li>Брокерские услуги;</li>
							<li>Консультации по инвестированию;</li>
							<li>Открытие счетов для физических лиц;</li>
							<li>Открытие корпоративных счетов;</li>
							<li>Обслуживание дебетовых и кредитных карт в кронах и евро;</li>
							<li>Интернет-банк на английском языке.</li>
						</ul>
					</div>
				</div>
				<!-- POST -->
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Процедура открытия счета
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<p>
						Для открытия счета в банке клиенту необходимо предоставить следующий пакет документов:
						</p>
						<ul>
							<li>Копии обоих паспортов (внутреннего и заграничного), подтверждение места жительства и банковская рекомендация всех физических лиц, имеющих отношение к открываемому счету: бенефициаров, директоров (в т.ч. номинальных), акционеров (в т.ч. номинальных), распорядителей по счету.</li>
							<li>CV на бенефициаров и распорядителей счетом.</li>
							<li>Оригиналы апостилированных документов компании (свидетельство о регистрации, резолюции о назначении директоров от первого до последнего, устав, учредительный договор, доверенности и резолюции по форме банка (если в компании номинальный директор).</li>
						</ul>
						<p>
						Подробную информацию о процедуре открытия счета вы можете получить у юристов компании Атласрег, связавшись с нами по телефону. 
						</p>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>
</section>
<!-- /.GRID SECTION -->

</div>
<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>