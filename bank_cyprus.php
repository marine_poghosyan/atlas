<?php
include 'header_inner.php';
?>
		
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights" data-animated="fadeInLeftBig" data-animation-delay="400">BANK OF CYPRUS (КИПР)</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="index.html">Главная</a>
					</li>
					<li>
						<a href="open_account.php">Открытие банковских счетов </a>
					</li>
					<li class="typo-dark">BANK OF CYPRUS (КИПР)</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- PAGE HEADING SECTION -->
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
	<div class="row">
		<div class="col-md-9 top-margin-md">
		<p class="text-justify">
		Сайт банка – <a href="www.bankofcyprus.com">www.bankofcyprus.com</a> <br>
Расположение банка – Кипр <br>
Группа компаний Банка Кипра является крупной международной холдинговой структурой, обладает более чем вековой историей развития и занимает лидирующие позиции в области оказания банковских и финансовых услуг на Кипре.

		</p>
		</div>
		<div class="col-md-3 sidebar">
				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="bank_cyprus.php"  class="list-group-item active">Bank of Cyprus</a>
							<a href="bank_piraeus.php" class="list-group-item">Piraeus Bank</a>                                       
							<a href="bank_hellenic.php" class="list-group-item">Hellenic Bank</a>
							<a href="#" class="list-group-item">RCB Bank</a>
							<a href="bank_eurobank.php" class="list-group-item">Eurobank Cyprus</a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
	</div>
		<div class="row">
			<div class="col-md-9 ">
				<!-- POST -->
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Характеристики и тарифы Bank of Cyprus
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<table class="table">
						<thead>
						 
						</thead>
						<tbody>
						  <tr>
							<td>СРОК ОТКРЫТИЯ СЧЕТА</td>
							<td>14 - 20 рабочих дней</td>
						  </tr>
						  <tr>
							<td>ТИПЫ СЧЕТОВ</td>
							<td>Личные Корпоративные</td>
						  </tr>
						  <tr>
							<td>НАЗНАЧЕНИЯ СЧЕТОВ</td>
							<td>Сберегательные Расчетные</td>
						  </tr>
						  <tr>
							<td>ПЕРВОНАЧАЛЬНЫЙ ВЗНОС</td>
							<td>Активационный платеж: <br>
							300 EUR/USD - для юридических лиц <br>
							200 EUR/USD - для физических лиц
							</td>
						  <tr>
							<td>НЕСНИЖАЕМЫЙ ОСТАТОК</td>
							<td>Нет требований</td>
						  </tr>
						  <tr>
							<td>ПЕРСОНАЛ</td>
							<td>Русскоговорящий Англоговорящий</td>
						  </tr>
						  <tr>
							<td>УПРАВЛЕНИЕ СЧЕТОМ</td>
							<td>По факсу Интернет-банк (дигипасс)</td>
						  </tr>
						  <tr>
							<td>ТАРИФЫ</td>
							<td>
							Обслуживание счета в год (включая интернет банкинг и управление по факсу):
Для корпоративных и личных счетов 200 EUR
жегодное обновление клиентского файла (деятельность, обороты, контрагенты и др.) – 100 EUR min
Закрытие счета – 200 EUR
Хранение корреспонденции в банке в год:
- 200 EUR для физических лиц; 
- 300 EUR для юридических лиц.
Единовременная плата за устройство Диджипасс 40 EUR
Конвертация валют: 0,5% (мин.5 EUR / макс. 400 EUR)

Исходящие платежи в EUR (в зависимости от суммы и страны отправителя платежа):
В EUR: Комиссия за транзакцию - 0.15% min 5 EUR / max 400 EUR + Обработка 12 EUR
В другой валюте: Комиссия за транзакцию - 0.15% min 5 EUR / max 400 EUR + Обработка 12 EUR
Входящие платежи в EUR:
До 10 000 EUR без комиссии
10 000 EUR - 50 000 EUR: комиссия 4 EUR
Более 50 000 EUR: 30 EUR
Входящие платежи в другой валюте:
До 2 000 EUR: комиссия 2 EUR
2 000 EUR - 50 000 EUR: комиссия 10 EUR
Более 50 000 EUR: комиссия 40 EUR

							</td>
						  </tr>
						  <tr>
							<td>ПЛАСТИКОВЫЕ КАРТЫ</td>
							<td>
							Дебетовые карты, выпуск в EUR, USD, GBP, CHF
Годовое обслуживание карт в EUR
Для личных счетов:
- Visa Classic, Visa Electron – бесплатно; 
- Visa Gold – 42,5 EUR; 
- Visa Platinum – 85 EUR
Для корпоративных счетов: Visa Business Card – 25 EUR
Годовое обслуживание карт в USD
Для личных счетов:
- Для личных счетов: Visa Electron – бесплатно 
- Visa Classic – 20 USD; 
- Visa Gold – 50 USD 
- Visa Platinum – 100 USD;
Для корпоративных счетов: Visa Business Card - 30 USD
Гарантийный депозит – нет требований, кроме карты Visa Platinum - USD 10.000 или эквивалент
Снятие наличных: Евро – EUR 2,7 за транзакцию, Доллары – 3,33% (мин. USD 5,7 ) + USD 3,5
Оплата товаров и услуг – без комиссии.

							</td>
						  </tr>
						  
						</tbody>
					</table>
					</div>
				</div>
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Особенности и сервисы банка
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<p>
						Обслуживание зарубежных клиентов ведется в четырех специализированных Международных Бизнес-Центрах, расположенных на Кипре в Лимассоле, Никосии и Ларнаке.
						</p>
						<ul>
							<li>Брокерские услуги;</li>
							<li>Консультации по инвестированию;</li>
							<li>Открытие счетов для физических лиц;</li>
							<li>Открытие корпоративных счетов;</li>
							<li>Обслуживание дебетовых и кредитных карт в кронах и евро;</li>
							<li>Интернет-банк английском.</li>
						</ul>
					</div>
				</div>
				<!-- POST -->
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Процедура открытия счета в Bank of Cyprus
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<p>
						Для открытия счета в банке клиенту необходимо предоставить следующий пакет документов:
						</p>
						<ul>
							<li>копии обоих паспортов (внутреннего и заграничного), подтверждение места жительства и банковская рекомендация всех физических лиц, имеющих отношение к открываемому счету: бенефициаров, директоров (в т.ч. номинальных), акционеров (в т.ч. номинальных), распорядителей по счету.</li>
							<li>CV на бенефициаров и распорядителей счетом.</li>
							<li>Оригиналы апостилированных документов компании (свидетельство о регистрации, резолюции о назначении директоров от первого до последнего, устав, учредительный договор, доверенности и резолюции по форме банка (если в компании номинальный директор).</li>
							
						</ul>
						<p>
						Подробную информацию по процедуре открытия счета в Bank of Cyprus вы можете получить у юристов компании Атласрег связавшись с нами по телефону.
						</p>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>
</section>
<!-- /.GRID SECTION -->
<section class="theme-bg top-padding-sm no-bottom-padding typo-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="text-center" data-animation="fadeInLeft">
					<ul class="list-inline">
						<li><h2 class="section-titles">ПОЛУЧИТЕ БЕСПЛАТНУЮ ПОМОЩЬ</h2></li>
						<li class="left-padding-sm"><a class="btn btn-sm btn-light btn-hv-light btn-rounded btn-default" href="contact-us.html">связаться сейчас</a></li>
					</ul>
				   
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>