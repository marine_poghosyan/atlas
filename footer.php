 <!-- /.GRID SECTION -->
<section class="theme-bg top-padding-sm no-bottom-padding typo-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="text-center" data-animation="fadeInLeft">
					<ul class="list-inline">
						<li>
							<h2 class="section-titles">Закажите консультацию</h2>
							Оставьте нам заявку и мы предоставим Вам бесплатную консультацию
						</li>
						<li class="left-padding-sm"><a class="btn btn-sm btn-light btn-hv-light btn-rounded btn-default" href="contact-us.php#contacts">связаться сейчас</a></li>
					</ul>
				   
				</div>
			</div>
		</div>
	</div>
</section>
</div> <!--content-wrapper-->
 <!-- FOOTER SECTION -->
        <footer id="footer" class="footer dark-bg">
            <div class="footer-top">
                <div class="container">
                    <div class="row bg-no-repeat background-position-left">
                        <div class="col-md-6 col-sm-6">
                            <!-- LOGO -->
                            <img src="images/logo1.png" alt="logo" width="155" height="25">
                            <br>
                            
                            <div class="feature-box no-bottom-margin style-3">
                                <div class="icon-wrapper">
                                    <!-- ICON -->
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="inner-wrapper">
                                    <!--  PHONE NUMBER -->
                                    <p class="feature-description">8 800 100-43-65</p>
                                </div>
                            </div>
                            <div class="feature-box style-3">
                                <div class="icon-wrapper">
                                    <!-- ICON -->
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="inner-wrapper">
                                    <!-- EMAIL -->
                                    <p class="feature-description"><a class="" href="mailto:info@atlasreg.com">info@atlasreg.com</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- WIDGET -->
                            <h5 class="widget-title text-style text-uppercase">Наши услуги</h5>
                            <!-- LIST -->
                            <ul class="list-unstyled">
                                <li class="bottom-margin-vsm"><a href="ourstrategy.php">Регистрация иностранных компаний</a></li> 
                                <li class="bottom-margin-vsm"><a href="open_account.php">Открытие банковских счетов </a></li>
								<li class="bottom-margin-vsm"><a href="residence.php">Получение вида на жительство</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom footer-top-border">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="copyright">
                                &copy; Атласрег 2017. ВСЕ ПРАВА ЗАЩИЩЕНЫ.
								<!--created by...-->
                                <ul class="footer-bottom-links text-uppercase">
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <!-- SOCIAL SHARE -->
                            <ul class="footer-social-icons">
                                <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook text-darkest social-facebook-hvcolor"></i></a></li>
                                <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter text-darkest social-twitter-hvcolor"></i></a></li>
                                <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin text-darkest social-linkedin-hvcolor"></i></a></li>
                                <li class="pinterest"><a href="#" target="_blank"><i class="fa fa-pinterest text-darkest social-pinterest-hvcolor"></i></a></li>
                                <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus text-darkest social-google-plus-hvcolor"></i></a></li>
                                <li class="youtube"><a href="#" target="_blank"><i class="fa fa-youtube text-darkest social-youtube-hvcolor"></i></a></li>
                                <li class="dribbble"><a href="#" target="_blank"><i class="fa fa-dribbble text-darkest social-dribbble-hvcolor"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <a href="#" id="back-to-top" title="Back to top"></a>
        <!-- FOOTER SECTION -->
    </div>
    <!-- /. PAGE WRAPPER -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="js/jquery.mmenu.min.all.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/jquery.appear.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="js/count-to.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
	<script src="js/jquery.matchHeight.js"></script>
	<script src="js/script.js"></script>
	<script type="text/javascript" src="js/google.map.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-104649946-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>

</html>