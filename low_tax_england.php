<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Англия (LLP)</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Англия (LLP)</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a"><img class="img-responsive" width="960" height="640" src="images/flags/england.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Англия (LLP)
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="low_tax_cyprus.php"  class="list-group-item">Кипр</a>
							<a href="low_tax_switzerland.php" class="list-group-item">Швейцария</a>                                       
							<a href="low_tax_england.php" class="list-group-item active">Англия (LLC)</a>
							<a href="low_tax_scotland.php" class="list-group-item">Шотландия (LLC)</a>
							<a href="low_tax_jerci.php" class="list-group-item">Остров Джерси</a>
							<a href="low_tax_honkong.php" class="list-group-item">Гонконг</a>
							<a href="low_tax_singapore.php" class="list-group-item">Сингапур</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании в Великобритании	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Стандартный пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					<tr>
						  <td>Проверка названия</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Регистрация компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оплата 	госпошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление уставных документов</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Предоставление юридического адреса на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга агента на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Доставка документов курьерской службой</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление доп. комплекта документов под апостилем</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального директора на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального акционера на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					 
					  <tr>
						  <td>Общая стоимость</td>
						  <td>от 1450$</td>
						  <td>от 1650$</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>от 1200$</td>
						  <td>от 1400$</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Основные особенности компании в Великобритании	
				</span>								
				</h3>
				<p>
				
				<table class="table no-border text-center">
					
					<tbody>
						<tr>
						  <td>Организационно-правовая форма</td>
						  <td>Limited Liability Partnership (LLP)</td>
					  </tr>
					  <tr>
						  <td>Система права в стране</td>
						  <td>Английское общее право</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность реестра</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Раскрытие данных о бенефициаре</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к акционерам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество акционеров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности акционеров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускаются акционеры-юридические лица</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к директорам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество директоров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности директоров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли директор-юридическое лицо</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к секретарю</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требование по наличию секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли секретарь-юридическое лицо</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Уставный капитал и акции</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальный размер объявленного капитала</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Минимальный размер оплаченного капитала</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Срок оплаты объявленного капитала</td>
						  <td>Нет ограничений</td>
					  </tr>
					  <tr>
						  <td>Выпуск акций на предъявителя</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Налогообложение в Англии</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Налог на прибыль</td>
						  <td>23%</td>
					  </tr>
					  <tr>
						  <td>НДС</td>
						  <td>20%</td>
					  </tr>
					  <tr>
						  <td>Налог у источника (дивиденды, проценты)</td>
						  <td>20%</td>
					  </tr>
					  <tr>
						  <td>Налог на прирост капитала</td>
						  <td>20%</td>
					  </tr>
					  <tr>
						  <td>Дополнительные обязательные местные налоги</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Валютный контроль</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования по отчетности</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требования по сдаче отчетности</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требование по подаче Annual Return</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Открытый доступ к отчетности</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Дополнительная информация</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Членство в ОЭСР (OECD)</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Количество подписанных Соглашений об избежании двойного налогообложения</td>
						  <td>123</td>
					  </tr>
					  <tr>
						  <td>Наличие Соглашения об избежании двойного налогообложения с Россией</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Группа риска: ЦБ РФ N 1317-У</td>
						  <td>Да (группа 1</td>
					  </tr>
					  <tr>
						  <td>Группа риска: Минфин РФ Приказ N 108н</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации новой компании</td>
						  <td>2 недели</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Организационно-правовые формы компаний в Великобритании
				</span>								
				</h3>
				<p>
				Для регистрации оффшора в Англии нерезидентам доступны следующие форматы компаний:
				</p>
				<ul>
					<li>Limited. Это наиболее распространенная форма регистрации в Англии. Компания должна иметь минимум 1 директора и 1 акционера. Первый может быть только физическим лицом, в качестве второго может выступать и юридическое лицо. Гражданство значения не имеет. Присутствует возможность использовать номинальный сервис. Такие компании платят 20 % налога с чистой прибыли и ежегодно должны предоставлять отчетность.</li>
					<li>LLP. Это партнерство, которое состоит из 2 партнеров или больше. Они могут иметь любое гражданство, быть юридическими либо физическими лицами. Такой оффшор в Англии не платит подоходного налога. Но партнеры, получающие прибыль от компании, не освобождаются от налогообложения. Ежегодно компания должна предоставлять отчетность.li>
					<li>LP. Структура такой компании сходна с LLP. Отличие заключается в том, что компания освобождается от налогообложения в случае, если ее учредили нерезиденты Англии. Раз в год необходимо сдавать краткий Tax Return.</li>	
				</ul>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Преимущества регистрации компании в Великобритании
				</span>								
				</h3>
				
				<ul>
					<li>Высокий уровень доверия к компании с английской пропиской, ее престижность;</li>
					<li>наличие ряда договоров об устранении двойного налогообложения;</li>
					<li>доступность оффшорного режима для фирм LLP, LP;</li>
					<li>минимальный уставной капитал составляет всего 1 фунт;</li>
					<li>быстрая регистрация и ликвидация фирм;</li>
					<li>отсутствует необходимость нанимать директора-резидента;</li>
					<li>наличие номинального сервиса.</li>
				</ul>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>