<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Регистрация офшоров</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="index.html">Главная</a>
					</li>
					<li>
						<a href="ourstrategy.html">Наши услуги</a>
					</li>
					<li class="typo-dark">Регистрация офшоров</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-9 top-margin-lg">

				<!-- COLUMN 1/2 -->
				<div class="row bottom-margin-sm" data-animation="fadeInUp" data-animation-delay="400">
					<div class="col-sm-6">
						<img class="img-responsive" src="images/content/services/post-23.jpg" alt="post-service" width="750" height="683">
					</div>
					<div class="col-sm-6 bottom-margin">

						<div class="parallax-title-1">
							<h4 class="section-titles">Регистрация офшоров </h4>
						</div>
						<p class="">
						Мы рады предложить Вам услуги по регистрации оффшорных компаний в следующих юрисдикциях: 
						</p>

						<p><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i><span class="panel-list-1">Офшор Сейшелы </span></p>
						<p> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i><span class="panel-list-1">Офшор Белиз </span></p>
						<p> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i><span class="panel-list-1">Офшор BVI </span></p>
						<p> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i><span class="panel-list-1">Офшор Сингапур </span></p>
						<p> <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i><span class="panel-list-1">Офшор ОАЭ</span></p>

					</div>
				</div>
				<!-- /.COLUMN 1/2 -->
				<!-- PIE CHARTS STYLES -->
			</div>
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="offshore_seishel.html"  class="list-group-item">Офшор Сейшелы </a>
							<a href="offshore_beliz.html" class="list-group-item">Офшор Белиз </a>
							<a href="offshore_bvi.html" class="list-group-item">Офшор BVI</a>
							<a href="offshore_singapure.html" class="list-group-item">Офшор Сингапур </a>
							<a href="offshore_oae.html" class="list-group-item">Офшор ОАЭ</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>