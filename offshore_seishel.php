<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Сейшельские острова </h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Сейшельские острова </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a"><img class="img-responsive" width="960" height="640" src="images/flags/seishel.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Сейшельские острова
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="offshore_seishel.php"  class="list-group-item active">Сейшельские Острова</a>
							<a href="offshore_bahamas.php" class="list-group-item">Багамские Острова</a>                                       
							<a href="offshore_virgins.php" class="list-group-item">Британские Виргинские Острова</a>
							<a href="offshore_kaiman.php" class="list-group-item">Каймановы Острова</a>
							<a href="offshore_beliz.php" class="list-group-item">Белиз</a>
							<a href="offshore_panama.php" class="list-group-item">Панама</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании на Сейшельских островах	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Базовый пакет</th>
						<th>Стандартный пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					  <tr>
						  <td>Проверка названия</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Регистрация компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оплата госпошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление уставных документов</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Предоставление юридического адреса на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга агента на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Доставка документов курьерской службой</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление доп. комплекта документов под апостилем</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального директора на один год</td>
						  <td></td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального акционера на один год</td>
						  <td></td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Общая стоимость</td>
						  <td>450$</td>
						  <td>700$</td>
						  <td>850$</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>450$</td>
						  <td>600$</td>
						  <td>800$</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
				
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Основные особенности оффшорной компании на Сейшельских островах
				</span>								
				</h3>
				<p>
				
				<table class="table one-third">
					<thead>
					  <tr>
						<th>Параметры</th>
						<th>Значения</th>										
					  </tr>
					</thead>
					<tbody>
					  <tr>
						  <td>Тип компании</td>
						  <td>IBC (International Business Company)</td>
					  </tr>
					  <tr>
						  <td>Название</td>
						  <td>Должно оканчиваться словами "Ltd", "Limited", "Corporation", "Corp.", "Incorporated", "Inc" и др.</td>
					  </tr>
					  <tr>
						  <td>Корпоративное законодательство</td>
						  <td>The IBC Act (1994)</td>
					  </tr>
					  <tr>
						  <td>Уставной Капитал</td>
						  <td>100.000 USD (не требует оплаты)</td>
					  </tr>
					  <tr>
						  <td>Акции на предъявителя</td>
						  <td>Разрешены</td>
					  </tr>
					  <tr>
						  <td>Мин. количество акционеров</td>
						  <td>1 (может быть юр. лицом)</td>
					  </tr>
					  <tr>
						  <td>Номинальные акционеры</td>
						  <td>Разрешены</td>
					  </tr>
					  <tr>
						  <td>Мин. количество директоров</td>
						  <td>1 (может быть юр. лицом)</td>
					  </tr>
					  <tr>
						  <td>Номинальные директора</td>
						  <td>Разрешены</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность</td>
						  <td>Высокая. В стране закрытый регистр акционеров и директоров. Информация предоставляется только по решению суда. При использовании номинальных директоров и акционеров истинные владельцы компании вообще не могут быть установлены.</td>
					  </tr>
					  <tr>
						  <td>Проведение ежегодных собраний</td>
						  <td>Не требуется. При необходимости могут быть проведены в любой стране мира (возможно по телефону).</td>
					  </tr>
					  <tr>
						  <td>Сдача ежегодной отчетности</td>
						  <td>Не требуется</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Не требуется</td>
					  </tr>
					  <tr>
						  <td>Налогообложение</td>
						  <td>Отсутствует</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации и доставки документов</td>
						  <td>2 недели</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Список документов, необходимых для регистрации компании на Сейшельских островах
				</span>								
				</h3>
				<p class="bottom-padding-md">
				1) Заверенная копия паспорта.
Подходит как внутренний, так и заграничный паспорт. Заверение может быть выполнено нотариусом или иным юристом, менеджером банка, офицером полиции и другим официальным лицом.
<br>
2) Подтверждение адреса.
Счет за коммунальные услуги, выписка с банковского счета, выписка со счета кредитной карты, справка о месте жительства, выданная муниципальными органами, нотариусом или банком и другие аналогичные документы.
В качестве подтверждения адреса может быть также использована паспортная страница с пропиской (в том случае, если в качестве первичного удостоверения личности был представлен другой паспорт).

Кроме того, владельцу компании будет предложено подписать стандартную форму заказа и Условия сотрудничества.


				</p>
				
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
					Регистрация оффшорной компании на Сейшельских островах: пошаговое руководство
				</span>								
				</h3>
				<p>
				Шаг 1. Заполнение формы заказа и предварительные консультации.
Сроки: моментально.
Итак, Вы определились в Вашем желании приобрести компанию на Сейшелах. С чего начать? Проще и удобнее всего будет позвонить нам или отправить заявку на наш электронный адрес с указанием Вашего контактного номера, после чего мы свяжемся с Вами в ближайшее время. Наши специалисты помогут подобрать оптимальную структуру компании под Ваши запросы и нужды.</p>
<p>Шаг 2. Утверждение названия компании.
Сроки: один рабочий день.
В соответствии с законодательством Сейшельских островов название компании должно быть утверждено Регистрационной палатой. 
К названию компаний на Сейшелах предъявляются три простых требования:
- название должно быть уникальным;
- название не должно содержать запрещенных слов или указывать на лицензируемый род деятельности;
- название не должно указывать на связь оффшорной компании с Сейшелами или сейшельским правительством.</p>
<p>Шаг 3. Оплата.
Сроки: в зависимости от выбранного способа платежа.
Как только название компании утверждено, и все детали заказа согласованы, наши сотрудники выставят Вам инвойс. Оплатить инвойс Вы сможете посредством банковского перевода, кредитной картой через Интернет, через систему Western Union или наличными по согласованию с сопровождающим Вас менеджером.
Регистрация компании начнется сразу после оплаты инвойса.</p>
<p>Шаг 4. Регистрация компании.
Срок: один рабочий день.
В течение 24 часов Ваша компания будет официально зарегистрирована, получит юридический адрес и регистрационный номер.</p>
<p>Шаг 5. Оформление и отправка документов.
Сроки: от двух до пяти рабочих дней.
Уставные документы компании будут готовы через один-два рабочих дня после регистрации компании. В том случае, если требуется нотариальное заверение и апостилирование документов, может понадобиться дополнительно несколько дней.
Готовые документы будут отправлены курьером по указанному Вами адресу.</p>
<p class="bottom-padding-md">Шаг 6. Пост-регистрационные процедуры.
После того, как регистрация компании завершена, наши сотрудники свяжутся с Вами и попросят Вас подписать несколько документов (декларация об адресе хранения документации компании, подтверждение должности директора и т.д.).

				</p>
			</div>
		</div>
	</div>
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>