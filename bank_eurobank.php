<?php
include 'header_inner.php';
?>
		
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights" data-animated="fadeInLeftBig" data-animation-delay="400">​EUROBANK CYPRUS (КИПР)</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="index.html">Главная</a>
					</li>
					<li>
						<a href="open_account.php">Открытие банковских счетов </a>
					</li>
					<li class="typo-dark">​EUROBANK CYPRUS (КИПР)</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- PAGE HEADING SECTION -->
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
	<div class="row">
		<div class="col-md-9 top-margin-md">
		<p class="text-justify">
		Сайт банка –  <a href="www.eurobank.com.cy">www.eurobank.com.cy</a> <br>
Расположение банка – Кипр, Никосия <br>
Филиалы Группы Eurobank EFG ведут свою деятельность в Греции, Болгарии, Сербии, Румынии, Турции, Польше, Украине, Великобритании, Люксембурге и на Кипре. Группа Eurobank входит в состав EFG, международной банковской группы, представленной в 40 странах.
<br>
Кипрский филиал Eurobank EFG Cyprus Ltd специализируется в основном на бизнес-обслуживании международного уровня, предлагая своим клиентам широкий спектр высококачественных услуг.
<br>
Банк основан в 2007 году как филиал банка EFG Eurobank Ergasias S.A. В 2008 получил лицензию Центрального Банка Кипра на проведение операций в качестве дочернего банка EFG Eurobank Ergasias S.A
</p>
		</div>
		<div class="col-md-3 sidebar">
				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="bank_cyprus.php"  class="list-group-item">Bank of Cyprus</a>
							<a href="bank_piraeus.php" class="list-group-item">Piraeus Bank</a>                                       
							<a href="bank_hellenic.php" class="list-group-item">Hellenic Bank</a>
							<a href="#" class="list-group-item">RCB Bank</a>
							<a href="bank_eurobank.php" class="list-group-item active">Eurobank Cyprus</a>
						</div>
					</div>
					<!-- category-list -->
				</div>                                                      
			</div>
	</div>
		<div class="row">
			<div class="col-md-9 ">
				<!-- POST -->
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Основные характеристики и тарифы Eurobank Сyprus
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<table class="table">
						<thead>
						 
						</thead>
						<tbody>
						  <tr>
							<td>СРОК ОТКРЫТИЯ СЧЕТА</td>
							<td>
							3 - 7 рабочих дней

							</td>
						  </tr>
						  <tr>
							<td>ТИПЫ СЧЕТОВ</td>
							<td>Личные Корпоративные</td>
						  </tr>
						  <tr>
							<td>НАЗНАЧЕНИЯ СЧЕТОВ</td>
							<td>Сберегательно-инвестиционные Расчетные</td>
						  </tr>
						  <tr>
							<td>ПЕРВОНАЧАЛЬНЫЙ ВЗНОС</td>
							<td>Нет требований</td>
						  <tr>
							<td>НЕСНИЖАЕМЫЙ ОСТАТОК</td>
							<td>Нет требований</td>
						  </tr>
						  <tr>
							<td>ПЕРСОНАЛ</td>
							<td>Русскоговорящий Англоговорящий</td>
						  </tr>
						  <tr>
							<td>УПРАВЛЕНИЕ СЧЕТОМ</td>
							<td>
							По факсу (кодовые таблицы) <br> Интернет-банк
							</td>
						  </tr>
						  <tr>
							<td>ТАРИФЫ</td>
							<td>
							Обслуживание счета: бесплатно
Хранение корреспонденции в банке в год: 60 EUR
Интернет-банк: бесплатно
Факсовое управление: 0,5 EUR за страницу
Конвертация валют: 0,1 % (макс. 340 EUR)

Входящие платежи в EUR:
Переводы SEPA/SWIFT:
- до 1 000 EUR – без комиссии;
- 1 000 EUR - 5 000 EUR - 2 EUR;
- 5 000 EUR - 20 000 EUR - 4 EUR;
- 20 000 EUR - 50 000 EUR - 10 EUR;
- Свыше 50 000 EUR - 16 EUR.
Входящие платежи в иностранной валюте:
Переводы SEPA/SWIFT:
- до 1 000 EUR – без комиссии;
- 1 000 EUR - 5 000 EUR - 2 EUR + 0,1 % (min 5 EUR, max 340 EUR);
- 5 000 EUR - 20 000 EUR - 4 EUR + 0,1 % (min 5 EUR, max 340 EUR);
- 20 000 EUR - 50 000 EUR - 10 EUR + 0,1 % (min 5 EUR, max 340 EUR);
- Свыше 50 000 EUR - 16 EUR + 0,1 % (min 5 EUR, max 340 EUR);
Исходящие платежи
Переводы SWIFT:
- до 2 000 EUR - 5 EUR;
- от 2 000 EUR до 50 000 EUR – 25 EUR;
- свыше 50 000 EUR – 10 EUR + 0,1% (max 340 EUR)
Переводы SEPA:
- до 2 000 EUR – 5 EUR;
- от 2 000 EUR до 50 000 EUR – 10 EUR;
- свыше 50 000 EUR - 10 EUR + 0,1 % (min 5 EUR, max 340 EUR);
Исходящие платежи в иностранной валюте:
- До 2 000 EUR эквивалент: 8 EUR + 0,1% (max 340 EUR);
- Свыше 2 000 EUR эквивалент: 16 EUR + 0,1% (max 340 EUR);
- Комиссия банков-корреспондентов OUR – До 50 000 EUR - 25 EUR; Свыше 50 000 EUR - 25 EUR.

							</td>
						  </tr>
						  <tr>
							<td>ПЛАСТИКОВЫЕ КАРТЫ</td>
							<td>
							Visa Gold Выпуск: 20 EUR, 30 USD;
Снятие наличных: в банкоматах Bank of Cyprus на Кипре - без комиссии; В банкоматах других банков или из-за границы 4,00 EUR / 5,00 USD

							</td>
						  </tr>
						  
						</tbody>
					</table>
					</div>
				</div>
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Особенности и сервисы банка
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<p>
						Банк основан в 2007 году как филиал банка EFG Eurobank Ergasias S.A. В 2008 получил лицензию Центрального Банка Кипра на проведение операций в качестве дочернего банка EFG Eurobank Ergasias S.A. Спектр банковских услуг и товаров представлен следующими предложениями:
						</p>
						<ul>
							<li>планировании пенсионного обеспечения;</li>
							<li>Управление портфелями;управлении капиталом;</li>
							<li>обслуживании траста;</li>
							<li>разработка розничных продуктов для партнеров по банку;</li>
							<li>распределение активов капитала институциональных инвесторов.</li>
						</ul>
					</div>
				</div>
				<!-- POST -->
				<div class="row list-post">

					<div class="col-md-12" data-animation="fadeInUp" data-animation-delay="800">
						<div class="">
							<h4><a href="secrets-about-investment-they-are-still-keeping-from-you.html">
							Процедура открытия счета в Eurobank EFG Cyprus
							</a></h4>
						</div>
						
						<!-- CONTENT -->
						<p>
						Выезд за рубеж на встречу с банкиром не требуется - встреча распорядителя(-ей) по счету и бенефициара(-ов) с представителем банка проводится в офисе компании Атласрег.
						<br>
						Для открытия счета потребуется предоставить следующий пакет документов:
						</p>
						<ul>
							<li>Копии обоих паспортов (внутреннего и заграничного), подтверждение места жительства и банковская рекомендация всех физических лиц, имеющих отношение к открываемому счету: бенефициаров, директоров (в т.ч. номинальных), акционеров (в т.ч. номинальных), распорядителей по счету.</li>
							<li>CV на бенефициаров и распорядителей счетом.</li>
							<li>Оригиналы апостилированных документов компании (свидетельство о регистрации, резолюции о назначении директоров от первого до последнего, устав, учредительный договор, доверенности и резолюции по форме банка (если в компании номинальный директор))</li>
							<li>Оригиналы апостилированных сертификата соответствия и сертификата хорошего состояния (если компания старше 1 года).</li>
							<li>Оригиналы документов по выпуску акций (резолюция о выпуске, сертификаты акций, декларация траста (если акционером является номинал), реестр акционеров (желательно)).</li>
							<li>Информация по планируемому использованию счета.</li>
						</ul>
						<p>
						Подробную информацию по процедуре открытия счета в Eurobank вы можете получить у юристов компании Атласрег связавшись с нами по телефону.
						</p>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>
</section>
<!-- /.GRID SECTION -->

</div>
<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>