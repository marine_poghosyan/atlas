<?php
include 'header_inner.php';
?>
<!-- GRID SECTION -->
<!-- PAGE HEADING SECTION -->
<section class="page-header padding-sm page-title-left minimal light-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<!-- TITLE-->
				<h2 class="typo-lights">Кипр</h2>
				<!-- BREADCRUMB-->
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li>
						<a href="our_services.php">Услуги</a>
					</li>
					<li>
						<a href="ourstrategy.php">Регистрация иностранных компаний</a>
					</li>

					<li class="typo-dark">Кипр</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="sidebar-wrapper right-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-md-4 top-margin-lg">
				<!-- IMAGE -->
				<div class="post-image">
					<a"><img class="img-responsive" width="960" height="640" src="images/flags/cyprus.jpg" alt="" /></a>
					
				</div>
			</div>
			<div class="col-md-5 top-margin-lg">
				<div class="section-title">
					<!-- TITLE -->
					<h3 class="parallax-title-1"> 
					<span class="text-style">
						Кипр
					</span>								
					</h3>
				</div>
			   <p>
			   Сейшельские острова — это государство в Восточной Африке. Является независимой республикой.
Законодательство базируется на английском общем праве и гражданском праве Франции.
Экономика в первую очередь основывается на туризме и на рыболовстве. 

			   </p>
		   

			</div>
			
			<div class="col-md-3 sidebar">

				<div class="widget">
					
					<div id="MainMenu">
						<div class="list-group panel arrow-list list-style-1 border-none ">
							<a href="low_tax_cyprus.php"  class="list-group-item active">Кипр</a>
							<a href="low_tax_switzerland.php" class="list-group-item">Швейцария</a>                                       
							<a href="low_tax_england.php" class="list-group-item">Англия (LLC)</a>
							<a href="low_tax_scotland.php" class="list-group-item">Шотландия (LLC)</a>
							<a href="low_tax_jerci.php" class="list-group-item">Остров Джерси</a>
							<a href="low_tax_honkong.php" class="list-group-item">Гонконг</a>
							<a href="low_tax_singapore.php" class="list-group-item">Сингапур</a>
					</div>
					<!-- category-list -->
				</div>
				
			   
			</div>
			<!-- SIDEBAR END -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Регистрация компании на Кипре	
				</span>								
				</h3>
				<p>
				
				<table class="table">
					<thead>
					  <tr>
						<th>Услуга</th>
						<th>Стандартный пакет</th>
						<th>Полный пакет</th>									
					  </tr>
					</thead>
					<tbody>
					<tr>
						  <td>Проверка названия</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Регистрация компании</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оплата госпошлины</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление уставных документов</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Предоставление юридического адреса на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга агента на один год</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Доставка документов курьерской службой</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Оформление доп. комплекта документов под апостилем</td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального директора на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					  <tr>
						  <td>Услуга номинального акционера на один год</td>
						  <td></td>
						  <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
					  </tr>
					 
					  <tr>
						  <td>Общая стоимость</td>
						  <td>1200€</td>
						  <td>1400€</td>
					  </tr>
					  <tr>
						  <td>Стоимость годового обслуживания</td>
						  <td>1000€</td>
						  <td>1500€</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Основные особенности компании на Кипре.	
				</span>								
				</h3>
				<p>
				
				<table class="table no-border text-center">
					
					<tbody>
						<tr>
						  <td>Организационно-правовая форма</td>
						  <td>PLC (Private Limited Company)</td>
					  </tr>
					  <tr>
						  <td>Система права в стране</td>
						  <td>Английское право</td>
					  </tr>
					  <tr>
						  <td>Конфиденциальность реестра</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Раскрытие данных о бенефициаре</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования к акционерам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество акционеров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности акционеров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускаются акционеры-юридические лица</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к директорам</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальное количество директоров</td>
						  <td>1</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности директоров</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли директор-юридическое лицо</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Требования к секретарю</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требование по наличию секретаря</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требования к резидентности секретаря</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Допускается ли секретарь-юридическое лицо</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Уставный капитал и акции</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Минимальный размер объявленного капитала</td>
						  <td>Нет (обычно 1,000 EUR)</td>
					  </tr>
					  <tr>
						  <td>Минимальный размер оплаченного капитала</td>
						  <td>Нет ограничений</td>
					  </tr>
					  <tr>
						  <td>Срок оплаты объявленного капитала</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Выпуск акций на предъявителя</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Налогообложение на Кипре</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Налог на прибыль</td>
						  <td>12.5%</td>
					  </tr>
					  <tr>
						  <td>НДС</td>
						  <td>17%</td>
					  </tr>
					  <tr>
						  <td>Налог у источника (дивиденды, проценты)</td>
						  <td>5%</td>
					  </tr>
					  <tr>
						  <td>Налог на прирост капитала</td>
						  <td>0-20%</td>
					  </tr>
					  <tr>
						  <td>Дополнительные обязательные местные налоги</td>
						  <td>Налог на оборону</td>
					  </tr>
					  <tr>
						  <td>Валютный контроль</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td><b>Требования по отчетности</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Требования по сдаче отчетности</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Требование по подаче Annual Return</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Открытый доступ к отчетности</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Аудит</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td><b>Дополнительная информация</b></td>
						  <td></td>
					  </tr>
					  <tr>
						  <td>Членство в ОЭСР (OECD)</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Количество подписанных Соглашений об избежании двойного налогообложения</td>
						  <td>50</td>
					  </tr>
					  <tr>
						  <td>Наличие Соглашения об избежании двойного налогообложения с Россией</td>
						  <td>Да</td>
					  </tr>
					  <tr>
						  <td>Группа риска: ЦБ РФ N 1317-У</td>
						  <td>Да (группа 1)</td>
					  </tr>
					  <tr>
						  <td>Группа риска: Минфин РФ Приказ N 108н</td>
						  <td>Нет</td>
					  </tr>
					  <tr>
						  <td>Срок регистрации новой компании</td>
						  <td>2-3 недели</td>
					  </tr>
					</tbody>
				</table>
				</p>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Преимущества регистрации компании на Кипре

				</span>								
				</h3>
				<p>
				Кипр не принадлежит к классическим оффшорным юрисдикциям. Но здесь действует лояльная к зарубежным компаниям система налогообложения. Вы получаете возможность значительно снизить налоговую нагрузку. При этом компания на Кипре будет зарегистрирована в респектабельной юрисдикции, которая входит в белый список стран, соответствующих повышенным налоговым стандартам мира. Данный перечень был составлен ОЭСР, что уже само по себе поднимает престиж и статус кипрских оффшоров.
				</p>
				<ul>
					<li>Остров Кипр расположен в эпицентре транспортной инфраструктуры, которая связывает Европу, Африку и Ближний Восток. Перед оффшорами Кипра открываются перспективы налаживания международной торговли с минимальными транспортными расходами.</li>
					<li>Государство имеет развитую систему предоставления услуг, которая постоянно совершенствуется. В числе прочих направлений в оффшорной зоне Кипр отлажена надежная финансовая система. Владельцы компаний получают доступ к широкому набору современных инвестиционных инструментов.</li>
					<li>Компания будет работать в стране со справедливой правовой инфраструктурой. В случае возникновения каких-либо юридических разбирательств предприниматель имеет возможность защитить свои интересы в суде Кипра и получает гарантии объективного правосудия.</li>
										
				</ul>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Особенности налогообложения на Кипре
				</span>								
				</h3>
				<p>
				В оффшорной зоне Кипр действуют существенные налоговые льготы для международного бизнеса. Владельцы компаний получают ряд заметных выгод, поскольку на Кипре:
				</p>
				<ul>
					<li>ставка корпоративного налога очень низкая;</li>
					<li>входящие дивиденды не облагаются налогами;</li>
					<li>на пассивные доходы нерезидентных акционеров, кредиторов и держателей лицензий действует нулевая ставка налогообложения;</li>
					<li>прирост капитала компании тоже облагается по нулевой ставке.</li>
				</ul>
				<p class="bottom-padding-md">
				Еще один значимый аргумент в пользу регистрации компании на Кипре заключается в том, что данная страна подписала ряд договоров об исключении двойного налогообложения. Указанные соглашения подписаны и с государствами СНГ, включая Россию.
				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9 col-sm-12">
			<div class="section-title no-margin">
				<!-- TITLE -->
				<h3 class="parallax-title-1"> 
				<span class="text-style">
				Особенности регистрации компании на Кипре
				</span>								
				</h3>
				<p>
				Все выгоды оффшора на Кипре можно получить, если компания будет контролироваться и управляться на территории самого острова. Базовым требованием местных властей является наличие у нерезидентной фирмы постоянного представительства на Кипре. Среди других условий регистрации необходимо отметить следующие моменты:
				</p>
				<ul>
					<li>компания на Кипре должна иметь хотя бы одного акционера и одного директора, в качестве которых могут выступать физические и юридические лица;</li>
					<li>чтобы фирма попала под действие соглашений об исключении двойного налогообложения, ее директор должен быть кипрским резидентом;</li>
					<li>уставной капитал размером 1–5 тыс. евро не оплачивается при осуществлении регистрационных процедур;</li>
					<li>фирма обязана ежегодно сдавать отчетность о своей бизнес-деятельности.</li>
				</ul>
				<p class="bottom-padding-md"></p>
			</div>
		</div>
	</div>
	
	</div>
</section>


<!-- FOOTER SECTION -->
<?php
include 'footer.php';
?>